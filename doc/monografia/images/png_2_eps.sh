#!/bin/bash


for i in `ls -1 *.png`; do
	tmp=/tmp/$i.tiff
	echo "converting $i..."
	pngtopnm $i |pnmtotiff > $tmp
	tiff2ps -e $tmp > `echo $i|cut -d'.' -f1`.eps
	rm $tmp
done
