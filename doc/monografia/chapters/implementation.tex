%%%%%%%%%%%%%%%%%%%%%%
%
% INCLUSION OF RESULTS
%
%%%%%%%%%%%%%%%%%%%%%%

\chapter{Prototipação} % 3 pages
\label{chap:implementation}

Com o objetivo de exercitar o modelo de programação apresentado neste trabalho, um protótipo para os modos de acesso NVM.PM.VOLUME e NVM.PM.RAW foi desenvolvido. Um caso de uso foi elaborado e as ações necessárias para sua execução implementadas. A Tabela \ref{table:prot-usecase} apresenta o caso de uso.

Apesar de termos Persistent Memory disponíveis no mercado, optou-se pela simulação da Persistent Memory, visto que o objetivo principal do trabalho está no modelo de programação e não na tecnologia. Para a prototipação dos modos de acesso, optou-se pela utilização do sistema operacional Linux. Os principais motivos para esta escolha foram a familiaridade com este sistema operacional, a disponibilidade pública do código fonte, a extensa literatura e documentação disponível para este sistema operacional e também sua ampla utilização acadêmica e industrial. A Tabela \ref{table:prot-system} apresenta os principais hardwares e softwares utilizados no desenvolvimento do protótipo.

Vale salientar que, como informado no escopo da posição técnica da SNIA, apresentado na Seção \ref{sec:npm_scope}, o documento não descreve exaustivamente o modelo de modo que todas informações necessárias para implementação estejam disponíveis. Por este motivo, decisões foram tomadas em tempo de implementação, por exemplo a interface de administração dos modos de acesso.


\begin{table}[H]
\begin{center}
\caption{Principais hardwares e softwares utilizados no protótipo.}
\begin{tabular}{|p{3cm}|p{12cm}|}
\hline
Processador
&
Intel i3-2365M \footnote{http://ark.intel.com/PT-BR/products/70272/Intel-Core-i3-2365M-Processor-3M-Cache-1{\_}40-GHz acessado em 12 de novembro de 2014}
\\
\hline
Memória
&
4GB DDR3 1600MHz SH564128FH8NZQNSCG
\\
\hline
Distribuição
&
Debian GNU/Linux Wheezy 7.6
\\
\hline
Linux kernel
&
3.2.0-4
\\
\hline
gcc
&
4:4.7.2-1
\\
\hline
make
&
3.81-8.2
\\
\hline
glibc (mmap)
&
2.13-38
\\
\hline
grub
&
1.99-27
\\
\hline
\end{tabular}
\end{center}
\label{table:prot-system}
\end{table}



\begin{table}[H]
\begin{center}
\caption{Caso de uso Disponibilizar um volume PM para um programa em modo usuário}
\begin{tabular}{|p{3cm}|p{12cm}|}
\hline
Caso de uso & Disponibilizar um volume PM para um programa em modo usuário
\\
\hline
Ator
&
Administrador do sistema
\\
\hline
Pré-condições
&
\begin{itemize}
  \item O sistema possui uma Persistent Memory instalada e ativa.
\end{itemize}
\\
\hline
Entrada
&
\begin{itemize}
  \item O id do volume a ser criado.
  \item O tamanho do volume a ser criado.
\end{itemize}
\\
\hline
Fluxo de execução
&
\begin{enumerate}
  \item O administrador carrega o módulo de kernel responsável pelo modo de acesso NVM.PM.VOLUME;
  \item O administrador utiliza a ferramenta de criação de volume PM, passando como parâmetro o id e tamanho do volume a ser criado;
  \item O administrador carrega o módulo de kernel responsável pelo modo de acesso NVM.PM.RAW;
  \item O administrador utiliza a ferramenta de criação de um identificador do volume PM em modo usuário, passando como parâmetro o id do volume;
\end{enumerate}
\\
\hline
Fluxo Alternativo
&
2a. Caso o volume já exista, o sistema operacional deverá informar ao administrador com a mensagem "id already exists.";

2b. Caso o tamanho solicitado seja maior que o disponível, o sistema operacional deverá informar ao administrador com a mensagem "size invalid.";

2c. Caso o máximo de volumes possível tenha sido atingido, o sistema operacional deverá informar ao administrador com a mensagem "maxvolumes exceeded";

4a. Caso o identificador do volume PM em modo usuário já exista, o sistema operacional deverá informar ao administrador com a mensagem "id already exists.";
\\
\hline
Pós-condições
&
O identificador do volume PM está disponível para programas em modo usuário referenciá-lo.
\\
\hline
\end{tabular}
\end{center}
\label{table:prot-usecase}
\end{table}

\section{Simulando a Persistent Memory}

Como informado no Capítulo \ref{chap:pm_as_secondarymem}, uma Persistent Memory possui desempenho similar as memórias DRAM, vastamente utilizadas como memória principal. Neste trabalho, optou-se em simular a Persistent Memory na memória principal, mesma abordagem utilizada no projeto pmfs\footnote{https://github.com/linux-pmfs/pmfs acessado em 10 de novembro de 2014} da Intel. Através de um parâmetro de inicialização do kernel do Linux, informamos um intervalo da memória que deve ser tratado como área reservada, ou seja, este intervalo não está disponível para uso do sistema operacional. A Persistent Memory simulada tem capacidade de 512MB a partir do endereço físico 2GB, ou seja, o intervalo (0x80000000-0xA0000000).

No ambiente de desenvolvimento utilizado, o software grub\footnote{http://www.gnu.org/software/grub/} é responsável pela inicialização do sistema e, portanto, pela passagem do parâmetro de reserva de memória ao kernel. No arquivo /etc/default/grub, o parâmetro GRUB{\_}CMDLINE{\_}LINUX{\_}DEFAULT recebeu o valor ”memmap=’512M\$2G’”, conforme orientado na documentação do kernel linux\footnote{https://www.kernel.org/doc/Documentation/kernel-parameters.txt acessado em 12 de novembro de 2014}. Após aplicada a configuração, o sistema deve ser reiniciado. A área reservada pode ser consultada pelo administrador através da interface /proc/iomem.

\section{Protótipo do modo de acesso NVM.PM.VOLUME}

O modo de acesso NVM.PM.VOLUME foi implementado através de um módulo de kernel, nominado pmdrv. Ele tem a responsabilidade de criar os volumes PM e concedê-los a outros subsistemas do kernel linux. Sistemas de arquivos ou qualquer outro subsistema que pretenda acessar Persistent Memory precisam solicitar volumes PM para o módulo pmdrv. A partir do momento que o volume PM é retornado ao subsistema que o requisita, ele pode acessar a Persistent Memory diretamente, sem necessidade de intervenção do pmdrv.

Para armazenar as estruturas de dados que mantém a configuração referente ao uso da Persistent Memory, o módulo pmdrv precisa de uma área reservada, nominada PMDRV{\_}MGR, de tamanho PMDRV{\_}MGR{\_}SIZE. Esta área é instalada no primeiro byte da Persistent Memory. Apesar de executar em modo supervisor, o módulo pmdrv não pode acessar diretamente o endereçamento físico referente a Persistent Memory. O kernel tem seu próprio endereçamento lógico, portanto é necessário a criação de mapeamentos na MMU deste endereçamento para os endereços físicos referente a Persistent Memory. A função disponível no kernel para realizar este mapeamento é a ioremap, que recebe como parâmetro o endereço base e o tamanho do mapeamento. Em nosso caso, a chamada da função é realizada com ioremap(PMDRV{\_}MGR,PMDRV{\_}MGR{\_}SIZE).

\subsection{As estruturas de dados pmsuper e pmvolume}

O módulo pmdrv possui duas estruturas de dados principais: a pmsuper e a pmvolume, que podem ser vistas nas Figuras \ref{fig:pmsuper} e \ref{fig:pmvolume}, respectivamente. A estrutura de dados pmsuper é armazenada a partir do primeiro byte da área PMDRV{\_}MGR. Esta estrutura de dados é responsável por armazenar a lista de volumes PM, que podem existir até a quantidade PM{\_}MAXVOLUMES. Utiliza a implementação de lista circular disponibilizada pelo kernel do linux. O atributo active é utilizado para informar se a estrutura de dados já está inicializada. Caso não esteja, na inicialização de pmsuper é criado um volume PM de tamanho equivalente a toda área da Persistent Memory após a área reservada para a estrutura pmsuper. O atributo mgr{\_}used informa o próximo endereço disponível na área PMDRV{\_}MGR, e é utilizado na criação de novos volumes PM. O atributo count informa quantos volumes PM existem na Persistent Memory.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	%{images/pmsuper.eps}
	{images/pmsuper.eps}
\caption{Estrutura de dados pmsuper}
\label{fig:pmsuper}
\end{figure}

As estruturas de dados pmvolume também são armazenadas na área PMDRV{\_}MGR, e são responsáveis por representar um volume PM. Todo subsistema do kernel que deseja acessar uma Persistent Memory receberá as informações para acessá-la através desta estrutura de dados. Um volume PM é uma definição recursiva, ou seja, um volume PM pode ser uma lista de volumes PM. Neste caso, o atributo list é utilizado para ligar múltiplos volumes PM. Quando inicializado, esta lista aponta como antecessor e sucessor ela mesma, representando assim um volume PM independente. O atributo id é utilizado como identificador único do volume PM. Através deste identificador que outros subsistemas requisitarão um volume PM existente. Os atributos phys{\_}addr e size são de interesse do subsistema que acessará a área da Persistent Memory representada por este volume. O atributo flags possui informações úteis referentes ao volume, por exemplo se este volume está disponível ou já está em uso. Características específicas da tecnologia da Persistent Memory poderão utilizar este atributo para informar se são capazes de determinada funcionalidade, por exemplo se suportam a ação DISCARD{\_}IMMEDIATELY.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/pmvolume.eps}
\caption{Estrutura de dados pmvolume}
\label{fig:pmvolume}
\end{figure}

\subsection{Interface NVM.PM.VOLUME}

Das ações apresentadas no modelo NVM.PM.VOLUME, pmdrv implementa as ações GET{\_} RANGESET e EXISTS, necessárias no estudo de caso da Tabela \ref{table:prot-usecase}.

A ação GET{\_}RANGESET foi implementada através da função get{\_}rangeset, a interface para que outros subsistemas possam solicitar um volume PM. Ela recebe como entrada um identificador de volume PM e retorna um volume PM. O retorno é feito por valor e não por referência, evitando assim acesso direto de outros subsistemas à região PMDRV{\_}MGR. A função é simples, itera sob a lista de volumes PM até localizar o referenciado pelo identificador de entrada. Caso localize, marca o volume como utilizado e retorna as informações do volume PM. Caso não localize, retorna um volume PM nulo. Para que qualquer subsistema do kernel possa utilizar esta interface, a macro EXPORT{\_}SYMBOL, disponibilizada no kernel, publica esta função na tabela de símbolos do kernel do linux, que pode ser visualizada em /proc/kallsyms. A função get{\_}rangeset pode ser vista na Figura \ref{fig:get-rangeset}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/get-rangeset.eps}
\caption{Função get{\_}rangeset}
\label{fig:get-rangeset}
\end{figure}


A ação EXISTS é implementada através da função exists{\_}pmvolume. Recebe como entrada um identificador de volume PM e retorna 0 caso não encontre um volume com este identificador e 1 caso encontre. A função pode ser vista na Figura \ref{fig:exists}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/exists.eps}
\caption{Função exists{\_}pmvolume}
\label{fig:exists}
\end{figure}

\subsection{Configuração do administrador}

O apresentado nas seções anteriores é suficiente para implementarmos o previsto no modelo NVM.PM.VOLUME. No entanto, em um ambiente real, tarefas de administração precisam ser realizadas, por exemplo a criação de um volume PM. No modelo apresentado pela SNIA, os casos de uso assumem que o administrador do sistema já criou os volumes PM. Nesta implementação, trouxemos a responsabilidade pela criação dos volumes PM para o módulo pmdrv, através de uma das interfaces nativas de configuração de subsistemas do linux, o sistema de arquivos procfs. Para programas que executam em modo de usuário, é padrão a utilização de arquivos de configuração, porém para o kernel do linux isto não é o ideal\footnote{http://www.linuxjournal.com/article/8110 acessado em 12 de novembro de 2014}.

Três tarefas são necessárias para disponibilizar para o administrador do sistema uma interface ao pmdrv através do procfs: criar uma entrada no sistema de arquivos procfs, criar uma função de leitura de dados e criar uma função de escrita de dados. A entrada no sistema de arquivos será ligada as funções de leitura e escrita de dados. As funções de leitura e escrita de dados precisam definir um formato para apresentar e receber dados do administrador, respectivamente.

A entrada em procfs definida foi /proc/pm/volumes. Através deste arquivo, o administrador pode solicitar a criação de um volume PM e consultar os volumes existentes.

\paragraph{Criando um volume PM}

Para criar um volume PM, o administrador deve utilizar o programa echo, disponibilizado na bibloteca padrão do linux, com uma string como argumento no seguinte formato: “ID,TAMANHO,FLAGS”. Por exemplo, para criar um volume PM de ID=1, tamanho=1MB sem flags específicas, o administrador deve digitar o seguinte comando como superusuário: echo “1,1000000,0” > /proc/pm/volumes. A função de escrita vinculada a esta entrada procfs receberá esta string através de um buffer na memória principal, a tratará para consumir os parâmetros id,size e flags e utilizará a função set{\_}rangeset, que cria volumes PM. Erros são tratados e informados através do log dmesg. Caso a string informada não esteja no padrão, será emitido a mensagem “format error”. Caso a criação solicitada possua ID existente, será emitido a mensagem “ID already exists”. Caso o limite da quantidade de volumes tenha sido atingido, será emitido a mensagem “MAXVOLUMES exceeded”. Caso  o tamanho solicitado seja superior ao disponível ou inferior ao tamanho de página pré-definido pelo kernel como PAGE{\_}SIZE, será emitido a mensagem “SIZE invalid.” O tamanho deve ser, no mínimo de uma página, pois o mapeamento do volume PM para o programa em modo de usuário é feito na granularidade de páginas, e não de bytes. Mais detalhes na Seção \ref{sec:pmraw-mmap}. A execução do comando de criação de um volume pode ser visto na Figura \ref{fig:nvm-pm-volume-admin}.
    A remoção de volumes PM ocorre na descarga do módulo pmdrv.
    
\paragraph{Consultando volumes existentes}

Para consultar os volumes PM existentes, o administrador deve utilizar o programa cat, disponibilizado na biblioteca padrão do linux. Basta digitar o seguinte comando como superusuário: cat /proc/pm/volumes. A função de leitura vinculada a esta entrada procfs preparará uma string com todos volumes presentes na Persistent Memory e copiará esta string para um buffer que será acessado pelo programa cat. A execução do comando de consulta de volumes existentes pode ser visto na Figura \ref{fig:nvm-pm-volume-admin}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/nvm-pm-volume-admin.eps}
\caption{Criação e consulta de volume PM através do procfs}
\label{fig:nvm-pm-volume-admin}
\end{figure}

\section{Implementação do modo de acesso NVM.PM.RAW}

O modo de acesso NVM.PM.RAW foi implementado através de um módulo de kernel, nominado pmraw. Ele tem a responsabilidade de prover uma interface para que programas que executam em modo usuário possam acessar um volume PM diretamente, através da chamada de sistema mmap(), que implementa a ação NVM.PM.RAW.MAP do modelo. O programa precisa conhecer um identificador que faça referência a um volume PM, e para isso o módulo pmraw cria os dispositivos /dev/pmraw. Ainda, assim como no módulo pmdrv, o módulo pmraw também precisa fornecer uma interface de configuração para que o administrador possa criar os dispositivos /dev/pmraw.

\subsection{Os dispositivos /dev/pmraw}

Os dispositivos /dev/pmraw são do tipo char devices\footnote{http://www.xml.com/ldd/chapter/book/ch03.html acessado em 12 de novembro de 2014}, ou seja, orientados a caracter. Este tipo de dispositivo possui dois atributos principais: major e minor. O atributo major é utilizado para mapear o dispositivo a um módulo do kernel, em nosso caso o módulo pmraw. Sem este mapeamento, não seria possível vincular este dispositivo a função mmap especializada, que mapeia volumes PM. O atributo minor é de uso interno do módulo, e em nosso caso será utilizado como identificador do volume PM ao qual o dispositivo está vinculado. A criação destes dispositivos é realizada pelo administrador do sistema, como será apresentado nesta próxima subseção. A remoção destes dispositivos ocorre na descarga do módulo pmraw.

\subsection{Criando um dispositivo /dev/pmraw}

Assim como no módulo pmdrv, a interface de administração deste módulo ocorre pelo sistema de arquivos procfs. Na inicialização do pmraw, o arquivo /proc/pmraw é criado. Vale salientar que este módulo só pode ser carregado caso o módulo pmdrv já esteja carregado no kernel, em virtude da dependência do símbolo get{\_}rangeset e algumas macros. Quando um administrador deseja criar um dispositivo, ele utiliza o programa echo, com uma string como argumento no seguinte formato: “ID.”. Por exemplo, para criar o dispositivo /dev/pmraw1 o administrador deve digitar o seguinte comando como superusuário: echo “1.” > /proc/pmraw. A função de escrita vinculada a esta entrada procfs receberá esta string através de um buffer na memória principal, a tratará para consumir o parâmetro id e utilizará a função de criação de dispositivo disponibilizada pelo kernel, processo que será detalhado a seguir. Erros são tratados e informados através do log dmesg. Caso a string informada não esteja no padrão, será emitido a mensagem “format error”. Caso a criação solicitada possua ID existente, será emitido a mensagem “ID already exists”. Caso o limite da quantidade de dispositivos tenha sido atingido, será emitido a mensagem “MAXDEVICES exceeded”. A execução do comando de criação de um dispositivo pmraw pode ser visto na Figura \ref{fig:nvm-pm-raw-admin}. 

A criação do dispositivo /dev/pmraw envolve duas etapas: o registro no kernel e a criação do arquivo em sí. Para registro no kernel, é utilizado a função register{\_}chrdev, que recebe como parâmetro o nome do dispositivo a ser criado, por exemplo “pmraw1”, e as funções de operação que serão mapeadas a este dispositivo. É aqui que a função mmap() que será descrita na subseção 6.3.3 é mapeada ao dispositivo. Após o registro no kernel, ainda se faz necessário a criação do arquivo do dispositivo, que será utilizado como identificador do volume PM pelo programa em modo de usuário. Esta criação pode ser realizada manualmente pelo administrador do sistema através do programa mknod ou pelo subsistema udev do kernel. Optou-se em criar o dispositivo através do udev. Para isso, utiliza-se a função device{\_}create, que recebe como parâmetro, dentre outras coisas, os atributos major e minor. É aqui que o dispositivo criado é mapeado ao ID informado pelo usuário na execução do comando echo.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/nvm-pm-raw-admin.eps}
\caption{Criação de um dispositivo /dev/pmraw através do procfs}
\label{fig:nvm-pm-raw-admin}
\end{figure}

\subsection{A função pmraw{\_}mmap()}
\label{sec:pmraw-mmap}

De toda infraestrutura apresentada até agora, chegamos aquela que será interface direta para o programa que pretende acessar a Persistent Memory. A chamada de sistema mmap é padrão para os principais sistemas de arquivos, porém em nossa implementação ela precisa ser personalizada, pois irá mapear para o usuário não um intervalo da memória principal, mas sim da Persistent Memory. Quando um programa utiliza a chamada de sistema mmap(), é necessário um chaveamento de contexto para que o kernel possa atuar e fazer as devidas configurações de modo que o programa receba um intervalo de memória mapeado no espaço de endereçamento referente ao seu contexto de execução. Dentre as diversas atividades que o kernel faz, durante a execução de uma função chamada mmap{\_}region, ele executa uma função callback, que para arquivos em sistemas de arquivos é mapeada para função padrão mmap, porém para nossa implementação foi redefinida para o arquivo que representa o dispositivo /dev/pmraw durante o registro deste dispositivo no kernel, como visto na subseção anterior. Este é o momento que a função mmap definida em pmraw é executada.

A função mmap recebe dois parâmetros: o arquivo referente o dispositivo /dev/pmraw e uma estrutura de dados vma, que representa uma região do endereçamento de memória do programa que executou a função mmap. Ela precisa realizar as seguintes tarefas: capturar o id do volume PM desejado; obter informações a respeito do volume PM, a saber o endereço físico base e o tamanho do volume e mapear o volume PM para o espaço de endereçamento do programa que executou a função mmap. Como já mencionado, o id do volume PM está marcado no atributo minor do dispositivo. Ele é obtido através da execução da macro MINOR pré-definida no kernel. As informações do volume são obtidas através da interface get{\_}rangeset, disponibilizada pelo módulo pmdrv. Para mapear o volume PM para o espaço de endereçamento do programa, foi utilizado a função do kernel remap{\_}pfn{\_}range, que recebe como parâmetro o endereço base e tamanho da Persistent Memory bem como o endereço base do espaço de endereçamento do programa, extraído da estrutura de dados vma. Por questões de arquitetura, a função remap{\_}pfn{\_}range recebe os endereços na granularidade de páginas, portanto precisamos redefinir os endereços envolvidos em função de páginas. A Figura \ref{fig:pmraw-mmap} apresenta a função pmraw{\_}mmap(). A Figura \ref{fig:linux-io-stack-pmraw} apresenta os protótipos pmraw e pmdrv no subsistema do kernel do Linux responsável pela interação de entrada e saída com dispositivos de memória secundária, bem como o acesso direto à Persistent Memory pelo programa em modo usuário. O diagrama original foi criado por Werner Fischer e George Schonberger \footnote{http://www.thomas-krenn.com/de/wikiDE/images/d/da/Linux-io-stack-diagram{\_}v1.0.eps acessado em 14 de novembro de 2014}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/pmraw-mmap.eps}
\caption{Função pmraw{\_}mmap().}
\label{fig:pmraw-mmap}
\end{figure}

\begin{center}
\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/linux-io-stack-pmraw-small.eps}
\caption{PMRAW no kernel do Linux.}
\label{fig:linux-io-stack-pmraw}
\end{figure}
\end{center}


\section{Acesso à Persistent Memory através do modo de acesso NVM.PM.RAW}

Com o protótipo apresentado na seção anterior, o caso de uso \ref{table:prot-usecase} já pode ser executado. Nesta seção apresentaremos um programa exemplo que utilizará o volume criado no caso de uso. Trata-se de um programa que calcula a sequência de Fibonacci utilizando programação dinâmica, ou seja, a cada iteração, o resultado anterior é aproveitado. Esta implementação apresenta melhor desempenho que a implementação recursiva, em contrapartida demanda uma área de memória maior, caso seja necessário armazenar os resultados intermediários. Utilizando Persistent Memory, mesmo com uma falha no sistema, os cálculos anteriores poderão ser reaproveitados.

A Figura \ref{fig:fibonacci} apresenta trechos do programa fibonacci.c. Este programa recebe dois parâmetros, o primeiro deve informar um nome de arquivo e o segundo o número limite para o cálculo da sequência de Fibonacci. Se o arquivo informado for um /dev/pmraw, ele utilizará o modo de acesso NVM.PM.RAW e portanto a Persistent Memory.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/fibonacci.eps}
\caption{Trechos do programa fibonacci.c.}
\label{fig:fibonacci}
\end{figure}
