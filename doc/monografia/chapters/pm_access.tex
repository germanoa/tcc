\chapter{Acesso à Persistent Memory}
\label{chap:pm_access}
Como destacado no capítulo anterior, tratando-se de um dispositivo de armazenamento, a adaptação de sistemas de arquivos é a abordagem natural na utilização de Persistent Memory. Historicamente, a evolução das tecnologias de computação são implementadas com o cuidado de manter compatibilidade com o legado, e não poderia ser diferente neste caso. Em contrapartida, trata-se de uma oportunidade de propor mudanças para um melhor uso do recurso de hardware disponível. Uma possível mudança está na camada de software atual, que possui mecanismos que podem ser otimizados ou até eliminados.

Neste capítulo serão utilizados conceitos de gerencia de memória e sistemas de arquivos para apresentação de propostas de otimização para o uso de Persistent Memory através de sistemas de arquivos.

\section{Acesso à Persistent Memory através de um sistema de arquivos}
    Os programas atuais, ao persistirem informações, utilizam chamadas de sistema fornecidas pelo sistema operacional para acesso aos dispositivos de memória secundária. O método mais comum é a utilização de sistemas de arquivos, que abstraem a organização dos dados em blocos em uma estrutura lógica denominada arquivo. Para descrever as melhorias que devem ser aplicadas à implementação dos sistemas de arquivos quando utilizam Persistent Memory, utilizaremos o programa read-example.c, especificamente detalhes relacionados a utilização da função read(), conforme Figura \ref{fig:read-example}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]
	{images/read-example.eps}
\caption{Programa read-example.c}
\label{fig:read-example}
\end{figure}

O programa read-example.c recebe como parâmetro um nome de arquivo, faz algumas validações, abre este arquivo, lê os primeiros bytes até limite SIZE e fecha-o. Retorna quantos bytes foram lidos -1 ou algum código de erro. Daremos ênfase a função  read(), essência deste programa. O protótipo e semântica desta função podem ser vistos na Tabela \ref{table:semantica-syscall-table}.

Ao utilizar esta função, o programador deseja carregar bytes que estão em um arquivo provido por um sistema de arquivos e normalmente armazenado fisicamente em um dispositivo de memória secundária. Os bytes carregados devem ser salvos no buffer *buf, que foi pré-alocado pelo programador e reside na memória principal. O acesso aos bytes do arquivo é realizado através da chamada de sistema sys{\_}read(). O sistema de arquivos possui uma estrutura de dados chamada inode para o arquivo referenciado e, através desta estrutura, sabe a localização física dos bytes solicitados na memória secundária. Com esta informação, o sistema de arquivos pode se comunicar com a memória secundária para solicitar os dados desejados. Via de regra, o mecanismo de paginação é utilizado por sistemas operacionais modernos para fazer esta solicitação à memória secundária. Uma estrutura de dados chamada page cache, que reside na memória principal, mantém uma lista de páginas, tipicamente um conjunto de 4Kbytes. O sistema de arquivos deve, a cada invocação, verificar se as páginas referente aos dados solicitados já estão na page cache: caso estejam, o sistema operacional retorna o dado que já estava em memória; caso contrário, a unidade de gerenciamento de memória (MMU), presente no processador, emitirá um evento chamado page fault para o sistema operacional. No tratamento deste evento, o sistema operacional solicitará à memória secundária que salve a página referida na page cache e só então poderá retornar o dado para ser armazenado no buffer *buf alocado pelo programador. A Figura \ref{fig:read-page-cache} \footnote{http://duartes.org/gustavo/blog/post/page-cache-the-affair-between-memory-and-files/ acessado em 10 de novembro de 2014} ilustra este processo.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]
	{images/readFromPageCache.eps}
\caption{Acesso aos dados na memória secundária através da page{\_}cache}
\label{fig:read-page-cache}
\end{figure}

Este resumo é suficiente para o destaque de duas questões: (a) todo byte de uma memória secundária solicitado por um programa está armazenado em três áreas: no buffer pré-alocado pelo usuário que reside na memória principal, na page cache do sistema operacional que também reside na memória principal e na própria memória secundária; e (b) a comunicação entre processador e memórias secundárias endereçadas a bloco não é nativo e por este motivo a memória principal precisa atuar como ponte nesta comunicação.

\subsection{Eliminando o page cache}
\label{sec:no_page_cache}

O processador não tem conhecimento da memória secundária, para o processador existe apenas uma memória, acessível através de endereçamento a byte. Portanto, algum mecanismo precisa ser implementado para que o processador tenha acesso a dados na memória secundária. O mecanismo utilizado atualmente é a paginação, que gera uma sobrecarga desnecessária quando a memória secundária é uma Persistent Memory.

Uma proposta para eliminar a necessidade do page cache é fazer com que o kernel copie diretamente da Persistent Memory os dados para o buffer do processo. O mecanismo de page cache não é necessário quando utilizamos Persistent Memory pois o tempo de acesso desta é equivalente ao tempo de acesso à memória principal. A estratégia conhecida como execute-in-place (XIP)\footnote{http://elinux.org/Kernel-XIP acessado em 10 de novembro de 2014} serve de inspiração para correção deste comportamento. O XIP é uma técnica utilizada para que programas sejam executados diretamente de uma memória secundária. Por exemplo, quando o computador é ligado, o primeiro estágio do carregador de inicialização é um programa armazenado em uma memória flash NOR, e precisa ser executado a partir desta memória, pois a memória principal ainda não é conhecida pelo processador. Dentre outras coisas, este programa configura o acesso a memória principal.

Para eliminar o page cache, não queremos exatamente executar instruções armazenadas em uma memória secundária, mas carregar e armazenar dados. Apesar de utilização distinta, a semântica é a mesma: permitir que o processador tenha acesso direto aos dados, eliminando a necessidade de ponte através da memória principal. Uma adaptação do código XIP do kernel do linux está em desenvolvimento para que sistemas de arquivos possam acessar diretamente Persistent Memory. DAX\footnote{https://lkml.org/lkml/2014/2/25/460 acessado em 10 de novembro de 2014} é o nome dado a este novo componente dos sistemas de arquivos.

Eliminado o page cache, ainda temos o dado repetido em dois locais: no buffer que reside na memória principal e na memória secundária. A função read() utiliza um buffer na memória principal para carregar os bytes que deseja ler de um determinado arquivo. O objetivo é minimizar o tempo de acesso aos dados e por consequência obter melhor desempenho. O mesmo ocorre com a função write(). A partir do momento que a memória secundária possui tempo de acesso equivalente a memória principal, utilizar este buffer é uma sobrecarga desnecessária para o sistema. 

\subsection{Eliminando buffer na memória principal}
\label{sec:no_buffer_with_mmap}


Para entendermos como eliminar o buffer da memória principal, precisaremos analisar a semântica das instruções que o utilizam. Vejamos as funções read() e write(), interfaces de acesso convencionais em sistemas de arquivos para manipulação de dados, com suas respectivas chamadas de sistema. A Tabela \ref{table:semantica-syscall-table}\footnote{http://linux.die.net/man/2/ acessado em 10 de novembro de 2014} mostra os protótipos de cada uma destas funções e sua semântica:

\begin{table}[h]
\begin{center}
\caption{Protótipos e semântica das chamadas de sistema read,write,lseek,mmap}
\begin{tabular}{|p{5.5cm}|p{6.7cm}|}
\hline
Protótipo & Semântica\\
\hline
"ssize{\_}t read(int fd, void *buf, size{\_}t count);"
&
Lê do arquivo que está na memória secundária referente descritor fd, count bytes para a memória principal no endereço buf. Retorna o número de bytes lidos.
\\
\hline
ssize{\_}t write(int fd, const void *buf, size{\_}t count);
&
Escreve no arquivo que está na memória secundária referente descritor fd, count bytes da memória principal a partir do endereço buf. Retorna o número de bytes escritos.
\\
\hline
off{\_}t lseek(int fd, off{\_}t offset, int whence);
&
Reposiciona o offset do próximo byte a ser lido do arquivo referente descritor fd. Retorna o atual offset em relação ao início do arquivo.
\\
\hline
void *mmap(void *addr, size{\_}t length int prot , int flags , int fd, off{\_}t offset);
&
Cria um mapeamento entre o espaço de endereçamento virtual do processo e o arquivo referente descritor fd. Retorna um ponteiro para o primeiro byte deste mapeamento.
\\
\hline
\end{tabular}
\end{center}
\label{table:semantica-syscall-table}
\end{table}


Como já mencionado anteriormente, estas funções dependem de um buffer na memória principal para que o processador possa ter acesso aos dados. Uma alternativa para a função read() seria modificarmos sua semântica de modo que o buffer *buf não precisasse ser alocado previamente mas fosse alterado pela chamada de função e recebesse um ponteiro que permita acesso direto a memória secundária. O primeiro problema desta proposta é a falta de compatibilidade com códigos existentes, pois seria necessário excluir a alocação de memória prévia do buffer. Outro problema é que esta semântica não é suficiente para a função write(), visto que o buffer é utilizado para informar os dados de origem, por consequência não poderia ser também um ponteiro para o destino onde os dados devem ser salvos. Outra proposta seria modificarmos a função read() para ignorar a existência do buffer e retornar o ponteiro que permite acesso direto a memória secundária. Esta modificação também não é compatível com o protótipo da função write, pois apesar de podermos utilizar o buffer como ponteiro de origem dos dados a serem armazenados, seria necessário um segundo ponteiro com o endereço de destino, ou seja, um endereço com mapeamento direto para a memória secundária. A alternativa de reaproveitarmos as funções read() e write() não é viável.

Uma alternativa mais promissora é a utilização da função mmap(). A implementação convencional desta função não precisa de alocação prévia de uma área de memória como as funções anteriores, ela própria se encarrega disso. Ao invocá-la, o kernel reserva uma área de memória e mapeia-a para uma região entre a pilha e a heap do espaço de endereçamento do processo. É o método utilizado para que os processos possam acessar bibliotecas compartilhadas, carregadas para memória uma única vez. De qualquer forma, apesar de ocultar este trabalho, o problema de alocação de um buffer na memória principal permanece. Um sistema de arquivos que tem conhecimento de Persistent Memory pode adaptar a semântica desta função para que o mapeamento gerado no espaço de endereçamento virtual do processo seja diretamente relacionado com os endereços físicos que fazem referência ao arquivo na Persistent Memory. Com esta abordagem, conseguimos eliminar o buffer da memória principal sem a necessidade de alteração do programa, que permanece compatível com sistemas de arquivos que não utilizam Persistent Memory.

Outra vantagem da utilização do mmap() é a facilidade no acesso não sequencial aos bytes. Se a função read() estiver sendo utilizada para ler um arquivo, todo acesso não sequencial exige a correção do offset através da função lseek(). Utilizando mmap(), basta somar o offset ao ponteiro corrente, simplificando a programação.  Vale ressaltar que, como mencionado na Seção \ref{sec:pm_persistencia}, a persistência dos dados é assíncrona e, caso o programador precise garantir a persistência em determinado trecho do código, deverá executar uma instrução que solicite sincronismo. Neste caso, a instrução msync() pode ser utilizada, o que permite que mantenhamos o raciocínio utilizado até o momento. Outra alternativa seria utilizarmos a técnica write-through ao invés de write-back, porém o impacto no desempenho do sistema inviabiliza esta proposta.

Com a eliminação do page{\_}cache e do buffer na memória principal, o dado está em um local apenas: na Persistent Memory.

\section{Acesso à Persistent Memory sem a utilização de sistemas de arquivos}
\label{sec:pm_direct}

O acesso direto à Persistent Memory através da função mmap() possui suas desvantagens. Funcionalidades que dependam da infraestrutura provida pelo sistema de arquivos ficam limitadas, por exemplo o uso de journaling, que mantém um log das alterações no sistema de arquivos e permite atomicidade nas operações de escrita. Neste caso, a responsabilidade por manter a atomicidade nas operações de escrita e permitir que dados corrompidos durante desligamento abrupto do sistema possam ser recuperados passa a ser do programa que os mantém, e não mais do sistema de arquivos.

A entrega da responsabilidade da gestão do armazenamento dos dados para uma aplicação não é nenhuma novidade. A persistência de dados através de sistemas de arquivos é vastamente utilizada e suficiente para maioria dos programas, porém existem casos que uma personalização na manipulação dos dados pode representar melhora significativa no desempenho do sistema, por exemplo para o uso de banco de dados \citep{oracle_hp_direct}. Implementações como a apresentada neste artigo dependem de acesso direto ao dispositivo, sem a utilização de sistemas de arquivos. Um outro sistema, projetado para um objetivo específico, deve prover a organização e a interface de acesso aos dados, de modo a obter mais desempenho por conhecer em detalhes o comportamento do programa ou do dispositivo de armazenamento. Um exemplo de sistema deste tipo é o Automatic Storage Management (ASM) \footnote{https://docs.oracle.com/cd/B28359{\_}01/server.111/b31107/asmcon.htm{\#}OSTMG036 acessado em 10 de novembro de 2014} da Oracle, componente recomendado por este fabricante para ambientes clusterizados.

Se o dispositivo de memória secundária for uma Persistent Memory, este acesso direto pode ser realizado através de operações load/store. O endereçamento não orientado a bloco mas a byte interrompe a compatibilidade natural com os dispositivos legados porém permite novos paradigmas de acesso e armazenamento de dados não possibilitados antes. Um programador pode agora, por exemplo, referenciar um intervalo de endereços de uma Persistent Memory através de um array de inteiros e manipulá-los como se o fizesse na memória principal. A utilização da função mmap() de sistemas de arquivos pode ser adaptada para este acesso direto, conforme apresentado nas seções anteriores, porém uma aplicação pode querer manipular dados em todo volume, e não apenas em um arquivo do volume. Neste caso, uma solução é eliminar a abstração de um sistema de arquivos e apresentar um volume inteiro para ser acessado diretamente pela aplicação.  Mnemosyne \citep{mnemosyne} e Eucalyptus \citep{eucalyptus} são exemplos de proposta de acesso direto à Persistent Memory, sem a utilização de sistemas de arquivos. Através de primitivas próprias, o programador pode acessar regiões de memória persistente sem a dependência de um sistema de arquivos.

No próximo capítulo será apresentado o modelo de programação proposto pela SNIA para definir o comportamento recomendado dos softwares que suportam NVM. Neste documento existe uma seção exclusiva para Persistent Memory, a qual será dada destaque. Vale ressaltar que este modelo possui uma interface de acesso através de sistemas de arquivos porém não há especificação definida para uma interface de acesso direto à NVM. No Capítulo \ref{chap:nvm_pm_raw} uma proposta para um modelo de acesso direto à Persistent Memory é apresentada.
