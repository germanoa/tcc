%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% INCLUSION OF METHODOLOGY
%
%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Modo de acesso NVM.PM.RAW} % 8-10 pages
\label{chap:nvm_pm_raw}

O Capítulo \ref{chap:snia_npm} apresentou os modos de acesso modelados pela SNIA. Dois destes modos são exclusivos para acesso à Persistent Memory, sendo um deles o modo de acesso com interface para os programas que executam em modo usuário, o NVM.PM.FILE. Através das chamadas de sistemas nativas de um sistema de arquivos, programas em modo usuário podem ter acesso à Persistent Memory.

No entanto, acessar a Persistent Memory através das chamadas de sistemas nativas dos sistemas de arquivos não explora ao máximo o uso desta memória. Como já discutido na Seção \ref{sec:no_page_cache}, os sistemas operacionais atuais estão projetados para disponibilizar o acesso aos dados na memória secundária utilizando a memória principal, causando uma sobrecarga desnecessária quando a memória secundária é uma Persistent Memory. Mesmo que as otimizações propostas sejam realizadas, na melhor hipótese o dado permanece duplicado. Ainda, estruturas de dados que utilizam ponteiros, por exemplo listas, grafos e árvores, precisam ser serializadas para poderem ser salvas, pois precisam ser definidas em termos de bloco. Utilizando Persistent Memory e fixando o endereço virtual a ser mapeado, configuração já permitida nas atuais implementações da chamada de sistema mmap(), estas estruturas de dados podem ser recuperadas após o encerramento do processo ou até mesmo após uma falha. Por estes motivos, acessar a Persistent Memory através da chamada de sistema de mapeamento de arquivo em memória é uma ótima alternativa caso a otimização do desempenho seja importante.

A utilização da chamada de sistema mmap() implica na perda de funcionalidades de consistência e tolerância a falhas providas por sistemas de arquivos. Por exemplo, o sistema de arquivos ext3 está configurado, por padrão, para sincronizar alterações nos dados e metadados a cada 5 segundos. Em caso de falha neste intervalo de tempo, os dados podem ser restaurados para o último estado consistente através do mecanismo journaling, fornecido pelo módulo Journaling Block Device layer (JBD)\citep{ext3doc}. De qualquer forma, a vantagem em desempenho na utilização de mapeamento de memória direto, como mencionado na Seção \ref{sec:pm_direct}, faz com que programas interessados optem em absorver a responsabilidade pela consistência e tolerância a falhas dos dados armazenados pela contrapartida de poder manipulá-los conforme decidir.

%Neste paragrafo a justificativa para NVM.PM.RAW.
A Seção \ref{sec:nvm_pm_file} apresenta a ação NVM.PM.FILE.MAP, que permite o mapeamento de um arquivo no espaço de endereçamento do programa, de modo que o dado possa ser acessado através de operações load/store. A vantagem desta abordagem é a compatibilidade com o modo de acesso existente, visto que os sistemas operacionais atuais já disponibilizam chamada de sistema que implementa esta ação, precisando apenas adequá-la conforme discutido na Seção \ref{sec:no_buffer_with_mmap}. Em contrapartida, o administrador precisará ter cuidados adicionais para este arquivo, garantindo por exemplo que ele não será movido para outro sistema de arquivos que não está instalado em uma Persistent Memory. Ainda, configurações típicas de um sistema de arquivos, como a mencionada no parágrafo anterior, deixam de ser necessárias, afinal a responsabilidade pela consistência e tolerância dos dados passa para o programa em modo usuário. A ativação destas configurações pode gerar sobrecarga desnecessária. Visto que um programa em modo usuário assumirá tarefas que tipicamente são realizadas por sistemas de arquivos, com o objetivo de otimizar o desempenho do acesso aos dados organizando-os de modo personalizado e adequado a sua demanda, a criação de uma interface direta para Persistent Memory torna-se oportuna.

Este trabalho propõe o modo de acesso NVM.PM.RAW, que provê, para aplicações que executam no modo usuário, interface de acesso direto para um volume PM através da chamada de sistema mmap(). A chamada de sistema possui a mesma sintaxe que a implementada por sistemas de arquivos, porém com semântica própria. Ao invés de mapear um arquivo para uma área na memória principal, mapeia no espaço de endereçamento do usuário um volume PM. O modelo utilizado para a apresentação deste modo de acesso utiliza a estrutura base utilizada pela posição técnica da SNIA, que foca na descrição do comportamento comum que dispositivos NVM devem ter ao serem expostos através das múltiplas interfaces especificas de cada sistema operacional.

\section{O modelo}

O modo de acesso NVM.PM.RAW define o comportamento da interface de programação a ser utilizada pelos programas que executam em modo usuário e que acessam a Persistent Memory sem a utilização de um sistema de arquivos. Assim, quando o programa em modo usuário deseja acessar diretamente um volume PM, a ação NVM.PM.- RAW.MAP disponibiliza em seu espaço de endereçamento um mapeamento direto para a Persistent Memory. As implementações deste modelo deverão informar ao programador como referenciar o volume PM.

Vale destacar que o componente responsável pela implementação deste modo de acesso não tem autonomia para autorizar o acesso ao volume PM, mas deve solicitá-lo ao componente que implementa o modo de acesso NVM.PM.VOLUME, procedimento idêntico ao realizado no modo de acesso NVM.PM.FILE. A Figura \ref{fig:nvm-pm-raw} apresenta um exemplo de acesso no modo NVM.PM.RAW. As ações disponibilizadas neste modelo são apresentadas na Tabela \ref{table:nvm-pm-raw-actions}. Os atributos deste modo de acesso estão apresentados na Tabela \ref{table:nvm-pm-raw-attr}.


\begin{figure}[H]
\centering
\includegraphics[scale=0.6]
	{images/nvm-pm-raw.eps}
\caption{Exemplo de uso do modo de acesso NVM.PM.RAW}
\label{fig:nvm-pm-raw}
\end{figure}

\begin{center}
\begin{table}[H]
\caption{Ações do modo de acesso NVM.PM.RAW}
\small
\begin{tabular}{|p{2cm}|p{4cm}|p{4cm}|p{4cm}|}
\hline
Ação & Propósito & Entrada & Saída\\
\hline
MAP
&
Mapeia um volume PM no espaço de endereçamento do usuário.
&
Um identificador para o volume PM.
&
O endereço base do volume PM ou nulo caso o volume não exista ou esteja em uso.
\\
\hline
SYNC
&
Sincronizar conteúdo da memória cache para Persistent Memory.
&
Fornecida pela implementação nativa da função de mapeamento oferecida pelo sistema operacional, por exemplo msync().
&
Fornecida pela implementação nativa da função de mapeamento oferecida pelo sistema operacional, por exemplo msync().
\\
\hline
UNMAP
&
Para que o programa informe que não utilizará mais este mapeamento.
&
Um identificador para o volume PM.
&
Nulo ou o endereço base do volume PM atualmente mapeado caso não seja possível desfazer o mapeamento.
\\
\hline
OPTIMIZED
{\_}FLUSH
&
Mesmo propósito da ação SYNC, porém com a intenção de permitir otimizações adicionais, por exemplo permitindo sincronização de múltiplos intervalos de memória.
&
Idêntica a fornecida pela ação SYNC.
&
Idêntica a fornecida pela ação SYNC.
\\
\hline
GET
{\_}ERROR
{\_}EVENT
{\_}INFO
&
Prover informações que permitam que a aplicação tome decisão de recuperação de dados.
&
Assume-se que esta ação receberá a saída gerada por um evento controlado.
&
1 = Indica que a aplicação pode retomar sua execução a partir deste ponto de interrupção.

2 = Indica erro que permite que controle pela aplicação.

3 = Houve perda de dados.
\\
\hline
OPTIMIZED
{\_}FLUSH
{\_}AND
{\_}VERIFY
&
Mesmo propósito da ação OPTMIZED{\_}FLUSH, porém a sincronização do dado é verificada, fornecendo maior segurança quanto a persistência dos dados.
&
Idêntica a fornecida pela ação SYNC.
&
Idêntica a fornecida pela ação SYNC.
\\
\hline
\end{tabular}
\label{table:nvm-pm-raw-actions}
\end{table}
\end{center}



\begin{table}[H]
\begin{center}
\caption{Atributos do modo de acesso NVM.PM.RAW}
\begin{tabular}{|p{3cm}|p{5.3cm}|p{3.3cm}|}
\hline
Atributo & Propósito & Tipo\\
\hline
MAP
{\_}COPY
{\_}ON{\_}WRITE
{\_}CAPABLE
&
Indica que a opção MAP{\_}COPY{\_}ON{\_}WRITE é suportada pela ação MAP.
&
booleano
\\
\hline
INTERRUPTED
{\_}STORE
{\_}ATOMICITY
&
Indica se o dispositivo suporta atomicidade na operação store em caso de queda de energia.
&
booleano
\\
\hline
FUNDAMENTAL
{\_}ERROR
{\_}RANGE
&
Número de bytes que podem tornar-se indisponíveis em virtude de um erro no dispositivo. Dados podem ser organizados em função deste valor de modo que, no erro de um intervalo, apenas uma informação seja perdida.
&
inteiro
\\
\hline
FUNDAMENTAL
{\_}ERROR
{\_}RANGE
{\_}OFFSET
&
Deslocamento para alinhar os intervalos informados no atributo anterior.
&
inteiro
\\
\hline
OPTIMIZED
{\_}FLUSH
{\_}CAPABLE
&
Indica se o dispositivo suporta a ação OPTIMIZED{\_}FLUSH.
&
booleano
\\
\hline
OPTIMIZED
{\_}FLUSH
{\_}AND
{\_}VERIFY
{\_}CAPABLE
&
Indica se o dispositivo suporta a ação OPTIMIZED{\_}FLUSH{\_}AND{\_}VERIFY.
&
booleano
\\
\hline
\end{tabular}
\end{center}
\label{table:nvm-pm-raw-attr}
\end{table}

\section{Caso de uso}

Um exemplo de uso deste modo de acesso é  o caso de uso descrito na Tabela \ref{table:nvm-pm-raw-usecase}. Um administrador de banco de dados deseja criar um novo índice no sistema através do módulo responsável pela gestão dos índices. Este módulo tem acesso a um volume PM, mapeado pela ação  NVM.PM.RAW.MAP.


\begin{table}[H]
\begin{center}
\caption{Caso de uso Criar um índice de banco de dados em um volume PM}
\begin{tabular}{|p{2.5cm}|p{12.5cm}|}
\hline
Caso de uso & Criar um índice de banco de dados em um volume PM
\\
\hline
Ator
&
Um Administrador de banco de dados
\\
\hline
Pré-condições
&
\begin{itemize}
  \item O administrador do sistema já criou um volume PM.
  \item O banco de dados possui uma referência a este volume PM e já o populou com os dados iniciais.
  \item O volume PM já está mapeado para o banco de dados através da ação NVM.PM.RAW.MAP.
  \item O banco de dados conhece Persistent Memory e possui subsistema responsável pela consistência e tolerância a falhas dos dados
\end{itemize}
\\
\hline
Entrada
&
\begin{itemize}
  \item O índice a ser criado.
\end{itemize}
\\
\hline
Fluxo de execução
&
1. O administrador utiliza o aplicativo de criação de indices, informando o índice a ser criado;

2. O subsistema responsável pela gestão dos índices recebe o pedido, trata-o e encaminha a solicitação de escrita dos dados que efetiva a criação do índice para o subsistema responsável pela escrita dos dados no volume PM;

3. O subsistema responsável pela escrita dos dados no volume PM faz os procedimentos necessários para garantir a consistência e tolerância a falhas dos dados;

4. O subsistema responsável pela escrita dos dados no volume PM escreve os dados no volume PM, através do mapeamento previamente realizado pela ação NVM.PM.RAW.MAP;

5. O subsistema responsável pela gestão dos índices utiliza a ação NVM.PM.RAW.SYNC para garantir a persistência dos dados;

6. O subsistema responsável pela gestão dos índices conclui a transação, garantindo a atomicidade da operação.
\\
\hline
Fluxo Alternativo
&
2a. Caso haja queda no sistema durante a operação, o subsistema responsável pela gestão dos índices solicita ao subsistema responsável pela escrita dos dados no volume PM a reversão da ação;
3,4a. O subsistema responsável pela escrita dos dados no volume PM utiliza os procedimentos de tolerância a falhas, recuperando o último estado consistente dos dados;
6a. O subsistema responsável pela gestão dos índices conclui o cancelamento da transação, garantindo a consistência dos dados. 
\\
\hline
Pós-condições
&
O novo índice está disponível na Persistent Memory.
\\
\hline
\end{tabular}
\end{center}
\label{table:nvm-pm-raw-usecase}
\end{table}
