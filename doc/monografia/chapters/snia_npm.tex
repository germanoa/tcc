\chapter{O modelo de programação proposto pela SNIA} % 8-10 pages
\label{chap:snia_npm}

A SNIA \footnote{http://www.snia.org/about acessado em 10 de novembro de 2014} é uma organização sem fins lucrativos com o objetivo de promover padronização e educação na área de armazenamento de informações. Possui aproximadamente 400 empresas membros, tais como Intel, HP, EMC e Oracle. No início de sua história, atuava contribuindo com desenvolvimento de tecnologias para outras organizações, tais como T11 \footnote{http://www.t11.org/ acessada em 10 de novembro de 2014} e T10 \footnote{http://www.t10.org/ acessada em 10 de novembro de 2014}, grupos de padrão para Fibre Channel e SCSI, respectivamente. Recentemente, começou a publicar seus próprios padrões, rotulando-os como posições técnicas da SNIA. Está organizada em grupos técnicos que, colaborativamente, desenvolvem estes padrões a serem utilizados por engenheiros de hardware e software de equipamentos de armazenamento.

No dia 21 de dezembro de 2013 a SNIA publicou um documento que define o comportamento recomendado para todo software que suporta memórias não voláteis (NVM). Trata-se da publicação  NVM Programming Model (NPM) \citep{NPM1}, que está na versão 1. Neste capítulo iremos apresentar uma visão geral deste documento e  o modelo de programação proposto, com maior ênfase a modelagem dedicada a Persistent Memory.

\section{Visão geral do documento NVM Programming Mode}
\label{sec:npm_overview}

O documento NVM Programming Model (NPM) é uma publicação da SNIA elaborada pelo grupo técnico NVM Programming, criado com o objetivo de acelerar a disponibilidade de software compatível com dispositivos NVM. É um documento agnóstico a fabricantes que se propõe a prestar suporte a todos membros associados. Ele não descreve uma API específica, mas o comportamento comum que dispositivos NVM devem ter ao serem expostos através das múltiplas interfaces específicas de cada sistema operacional. Está organizado em 10 capítulos e 4 anexos. Os primeiros 6 capítulos apresentam o escopo, definições, convenções utilizadas e informações gerais do modelo de programação proposto. Os últimos 4 descrevem os modos de acesso a NVM, cada modo descrito em termos de casos de uso, ações e atributos que informam as funcionalidades disponíveis.

\subsection{Definições básicas}
\label{def-snia-npm}

O documento apresenta algumas definições, tanto para apresentar novos conceitos quanto para determinar qual significado deve ser dado para um termo utilizado em diferentes contextos, por exemplo o conceito de volume. Foram selecionadas e traduzidas apenas as definições necessárias para o entendimento deste trabalho. A tabela \ref{table:def-basic-npm-table} apresenta estes conceitos.

\begin{table}[h]
\begin{center}
\caption{Definições básicas apresentadas no documento NVM Programming Model}
\begin{tabular}{|p{5.5cm}|p{6.7cm}|}
\hline
Termo & Definição\\
\hline
Arquivo mapeado em memória
&
Segmento de memória virtual com relação direta, byte a byte, de uma porção de um arquivo e endereços de memória.
\\
\hline
Módulo de dispositivo NVM orientado a bloco
&
Módulo que suporta as interfaces nativas de sistema operacional para acesso a dispositivos orientados a bloco.
\\
\hline
Volume NVM
&
Subconjunto de um ou mais dispositivos NVM, abstraídos por software como uma única entidade lógica
\\
\hline
Modelo de programação
&
Conjunto de interfaces de software que são utilizadas coletivamente para prover abstração de hardware com características similares
\\
\hline
\end{tabular}
\end{center}
\label{table:def-basic-npm-table}
\end{table}

\subsection{Escopo}
\label{sec:npm_scope}

Este documento está no formato de especificação, focado na interface que abstrai o hardware NVM para os softwares que pretendem acessá-lo. Esta abstração do hardware pode ser implementada de duas formas: como um componente de software do kernel, fornecendo um volume NVM, ou como um arquivo, disponibilizado através de um sistema de arquivos. Não é a intenção do documento descrever exaustivamente esta interface. Também não é intenção do documento tornar obsoleto os modos de acesso já implementados, mas estendê-los. Persistent Memory, que ainda não possui implementação de software largamente utilizada, tem dois modos de acesso exclusivos neste documento.

Está coberto por esta especificação o comportamento esperado na alocação, acesso e recuperação de dados armazenados em dispositivos NVM. Tarefas administrativas e de diagnóstico não estão cobertas.

Por fim, vale salientar que está no escopo deste documento apenas a definição semântica dos modos de acesso, permitindo definições sintáticas distintas. Esta decisão permite que, APIs existentes e distintas possam ser compatibilizadas com a semântica proposta sem a necessidade de alteração da sua sintaxe e por consequência da interface com os programas que a utilizam.

\subsection{Modos de acesso}

As próximas seções apresentarão os modos de acesso propostos pela SNIA. São quatro os modos de acesso presentes na especificação. Os dois primeiros, NVM.BLOCK e NVM.FILE, são modos utilizados quando os dispositivos NVM provêm acesso orientado a bloco. Tratam-se de modos de acesso já existentes e apenas extensões estão cobertas pela especificação. Neste trabalho, com foco em Persistent Memory, estes modos de acesso serão apresentados de forma geral e exemplificados com um caso de uso.

Os dois últimos modos de acesso, NVM.PM.VOLUME e NVM.PM.FILE, definem o modo de acesso a Persistent Memory. Para estes modos, serão apresentados as suas características principais, as ações e atributos definidos e um exemplo de acesso a Persistent Memory.

\section{NVM.BLOCK}

O modo NVM.BLOCK define o comportamento da interface de programação para implementação de dispositivos NVM orientados a bloco. Neste modo, dispositivos NVM são suportados por um módulo de sistema operacional que disponibiliza a interface de comandos para o dispositivo NVM. Todo acesso a NVM ocorre através desta camada de software. Dois casos de acesso são definidos: ou o programa, em modo usuário, poderá utilizar chamadas de sistema para acessá-lo bloco a bloco, diretamente sem a abstração de arquivos; ou os sistemas de arquivos farão uso da interface de programação NVM.BLOCK para acessar os dispositivos NVM. A especificação está limitada às extensões referentes a NVM, não trata da interface de programação já implementada pelos sistemas operacionais da atualidade. A Figura \ref{fig:nvm-block} apresenta um exemplo de uso deste modo de acesso. A interface de acesso está destacada em vermelho. O subsistema que a implementa está nominado como NVM block capable driver. Esta interface de acesso pode ser utilizada tanto em modo de usuário, por exemplo pelo programa Block-aware application, quanto em modo supervisor, por exemplo por um sistema de arquivos, também ilustrado na figura.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]
	{images/nvm-block.eps}
\caption{Exemplo de uso do modo de acesso NVM.BLOCK}
\label{fig:nvm-block}
\end{figure}

Na extensão proposta para este modo de acesso constam, dentre outras, as ações NVM. BLOCK.DISCARD{\_}IMMEDIATELY e NVM.BLOCK.EXISTS. Um exemplo de uso de  dispositivos NVM que dependem destas ações é a utilização de discos flash como cache de dados. Com esta cache, um sistema poderia, através de um módulo de gestão de cache, otimizar as requisições de acesso aos dados persistidos. Nas requisições de leitura, o módulo de gestão de cache consulta a presença do bloco solicitado no disco flash, retornando-o caso esteja presente ou buscando-o na área de armazenamento real do bloco caso contrário. Quanto as requisições de escrita, todas elas podem ser realizadas diretamente nos discos flash, permitindo um desbloqueio da operação de escrita mais rápido. Assíncronamente, o bloco é armazenado no seu destino real, nos dispositivos de armazenamento de maior capacidade e menor custo. A técnica de escrita descrita anteriormente é conhecida como write-back. O algoritmo de substituição dos blocos que devem estar nos discos flash pode ser configurado pelo administrador do sistema, de modo a permitir que o sistema esteja adequado a característica de requisições de acesso a blocos das aplicações que o utilizam. Um exemplo de algoritmo que pode ser utilizado é uma variação do algoritmo LRU, o LRU/2\citep{lru2}, que leva em conta os últimos dois acessos para priorizar blocos, evitando o processo de cache de blocos escaneados. Considerando este exemplo de uso, a capacidade do dispositivo NVM de disparar um evento através da ação NVM.BLOCK.DISCARD{\_}IMMEDIATELY é essencial para invalidarmos determinado bloco na cache. A necessidade de invalidação de um bloco na cache pode ocorrer, por exemplo, em clusters de servidores com discos compartilhados e caches distribuídas. A ação NVM.BLOCK.EXISTS é essencial para testar a presença de determinado bloco na cache. Também pode ser utilizada para informar se um bloco na cache precisa ser atualizado no disco que o armazena. A Tabela \ref{table:nvm-block-actions} apresenta as ações do NVM.BLOCK utilizadas neste exemplo. A Figura \ref{fig:nvm-block-cache} ilustra o uso de discos flash como cache de dados. Um exemplo de sistema real que utiliza este modo de acesso é o software Fast Cache \citep{fastcache} do fabricante EMC.

\begin{table}[h]
\begin{center}
\caption{Ações NVM.BLOCK.DISCARD{\_}IMMEDIATELY NVM.BLOCK.EXISTS}
\begin{tabular}{|p{2cm}|p{3.3cm}|p{3.3cm}|p{3.3cm}|}
\hline
Ação & Propósito & Entrada & Saída\\
\hline
DISCARD{\_}-
IMMEDIATELY
&
Força o descarte do conteúdo de um intervalo de blocos.
&
O endereço de um intervalo de blocos.
&
Retorna se a operação foi realizada com sucesso ou não.
\\
\hline
EXISTS
&
Informar se o bloco está alocado. Outros estados possíveis do bloco são: mapeado e não mapeado. Estes estados são úteis caso o mecanismo thin provisioning\citep{ibmthin} seja utilizado.
&
O endereço de um bloco.
&
Retorna o estado do bloco informado: alocado, mapeado ou não mapeado.
\\
\hline
\end{tabular}
\end{center}
\label{table:nvm-block-actions}
\end{table}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]
	{images/nvm-block-cache.eps}
\caption{Utilização de discos flash como cache de dados}
\label{fig:nvm-block-cache}
\end{figure}




\section{NVM.FILE}

O modo NVM.FILE define o comportamento da interface de programação tradicional para que programas possam acessar dados em dispositivos orientados a bloco: as chamadas de sistema providas por sistemas de arquivos. Não possui acesso direto aos dispositivos NVM, utiliza o modo NVM.BLOCK para acessá-los, como visto na seção anterior. A especificação deste modo de acesso também está limitada às extensões referentes a NVM, não descreve, por exemplo, o comportamento esperado das chamadas de sistema especificadas pelo padrão POSIX.1-2001 \footnote{http://www.unix.org/version3/ acessado em 10 de novembro de 2014}. A Figura \ref{fig:nvm-file} apresenta um exemplo de uso deste modo de acesso. A interface de acesso está destacada em vermelho. O sistema de arquivos é responsável por sua implementação, disponibilizando-a para os programas em modo usuário.


\begin{figure}[h]
\centering
\includegraphics[scale=0.6]
	{images/nvm-file.eps}
\caption{Exemplo de uso do modo de acesso NVM.FILE}
\label{fig:nvm-file}
\end{figure}

Na extensão proposta para este modo de acesso está a presença de diversos atributos, dentre eles o atributo PERFORMANCE{\_}BLOCK{\_}SIZE. Um exemplo de uso deste atributo é permitir que as operações nativas de leitura e escrita de um arquivo através de um sistema de arquivos seja otimizada, na aplicação, em função da característica física do dispositivo NVM. A otimização está na transformação da quantidade de dados a serem acessados em um valor múltiplo do tamanho de bloco ideal para aquele dispositivo NVM. A Tabela \ref{table:nvm-file-actions} apresenta a definição deste caso de uso.

\begin{table}[h]
\begin{center}
\begin{tabular}{|p{5.5cm}|p{6.7cm}|}
\hline
Caso de uso & Atualizar, otimizado a bloco, registros de um arquivo\\
\hline
Ator
&
Programa que abriu um arquivo e pretende atualizá-lo.
\\
\hline
Pré-condições
&
\begin{itemize}
  \item Um administrador criou o arquivo e informou ao programa seu identificador
  \item O arquivo não está em uso no início do caso de uso
\end{itemize}
\\
\hline
Entrada
&
O conteúdo do registro e sua localização relativa ao arquivo
\\
\hline
Fluxo de execução
&
\begin{enumerate}
  \item O programa usa a função nativa open(), passando como argumento uma flag que informa que o cache do sistema de arquivos precisa ser evitado
  \item O programa utiliza uma chamada de sistema disponibilizada pelo modo de acesso NVM.FILE que retorna o valor do atributo PERFORMANCE{\_}BLOCK{\_}SIZE
  \item O programa usa a função nativa write() de forma que o conteúdo do registro a ser atualizado esteja organizado em múltiplos de PERFORMANCE{\_}BLOCK{\_}SIZE
  \item O programa usa a função nativa sync() para garantir que os blocos atualizados estão persistidos no dispositivo NVM
  \item O programa usa a função nativa close() para fechar o arquivo
\end{enumerate}
\\
\hline
Fluxo alternativo
&
3a. Se a função write() retornar um erro de hardware, o programa deverá tentar executar novamente a função write(). Se falhar pela segunda vez, o programa deverá ser capaz de sinalizar que o bloco no dispositivo físico está com problemas e escrever o bloco em questão em outro local.
\\
\hline
Pós-condições
&
O registro está atualizado.
\\
\hline
\end{tabular}
\end{center}
\caption{Caso de uso Atualizar, otimizado a bloco, registros de um arquivo}
\label{table:nvm-file-actions}
\end{table}


\section{NVM.PM.VOLUME}

O modo de acesso NVM.PM.VOLUME define o comportamento da interface de programação a ser utilizada por módulos do sistema operacional que dependem de Persistent Memory para implementar suas funcionalidades. Sistemas de arquivos que pretendem acessar Persistent Memory precisam utilizar esta interface. Basicamente, o que  este modo de acesso provê é uma abstração em software, um volume PM, que possui as informações necessárias para que a Persistent Memory possa ser acessada. A partir do momento que o volume PM é retornado ao módulo do sistema operacional que o requisita, ele pode acessar a Persistent Memory diretamente, sem necessidade de intervenção do NVM.PM.VOLUME. A Figura \ref{fig:nvm-pm-volume} apresenta um exemplo de uso deste modo de acesso. O componente NVM PM capable driver é o responsável por implementar as interfaces de programação as quais terão suas semânticas definidas nesta seção. O PM-aware kernel component é o componente cliente do NVM.PM.VOLUME, quem recebe um volume PM e acessa-o conforme sua necessidade. Ainda, a figura ilustra que todo acesso a Persistent Memory é realizada diretamente pelo componente cliente. Vale destacar que este modo de acesso está disponível apenas em modo supervisor.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]
	{images/nvm-pm-volume.eps}
\caption{Exemplo de uso do modo de acesso NVM.PM.VOLUME}
\label{fig:nvm-pm-volume}
\end{figure}

O principal conceito na utilização deste modo de acesso é o volume PM.  Como visto na seção de definições relacionadas a esta especificação \ref{def-snia-npm}, um volume NVM é definido como um subconjunto de um ou mais dispositivos NVM, abstraídos por software como uma única entidade lógica. Por consequência, podemos assumir o mesmo conceito para volume PM, com a diferença que os dispositivos físicos envolvidos são todos Persistent Memory. A ação utilizada para entregar volumes PM é a NVM.PM.VOLUME.- GET{\_}RANGESET. Na apresentação desta ação, a especificação define uma estrutura base de representação de um volume PM: uma lista de lista de propriedades que deve ser retornada pela ação.  Cada lista de propriedades possui os seguintes atributos: endereço físico do primeiro byte, um tamanho em bytes, uma variável para definir o tipo de conexão e uma variável para definir o tipo de sincronismo. A especificação de uma lista de lista propriedades tem o objetivo de tratar o volume PM como uma entidade lógica de endereçamento contíguo porém implementado como uma lista de intervalos físicos que podem não ser contíguos na memória. Esta simples abstração permite uma melhor administração da Persistent Memory, evitando fragmentação externa neste dispositivo. A Tabela \ref{table:nvm-pm-volume-actions}  apresenta todas ações disponibilizadas por este modo de acesso e a Tabela \ref{table:nvm-pm-volume-attr} todos os atributos.

\begin{table}[h]
\begin{center}
\caption{Ações do modo de acesso NVM.PM.VOLUME}
\begin{tabular}{|p{3cm}|p{3.3cm}|p{3cm}|p{2.8cm}|}
\hline
Ação & Propósito & Entrada & Saída\\
\hline
GET
{\_}RANGESET
&
Retornar o volume PM referenciado.
&
Referência ao volume PM.
&
Volume PM.
\\
\hline
VIRTUAL
{\_}ADDRESS
{\_}SYNC
&
Executar ações específicas do dispositivo para sincronizar conteúdo e garantir durabilidade.
&
Um endereço virtual e um tamanho em bytes.
&
Não informado.
\\
\hline
PHYSICAL
{\_}ADDRESS
{\_}SYNC
&
Executar ações específicas do dispositivo para sincronizar conteúdo e garantir durabilidade.
&
Um endereço virtual e um tamanho em bytes.
&
Não informado.
\\
\hline
DISCARD
{\_}IF{\_}YOU
{\_}CAN
&
Informar o dispositivo que determinado intervalo de endereço não é mais necessário. Opcionalmente, o dispositivo pode não fazer nada.
&
Um endereço físico e um tamanho em bytes.
&
Não informado.
\\
\hline
DISCARD
{\_}IMMEDIATELY
&
Informar o dispositivo que determinado intervalo de endereço não é mais necessário. O dispositivo deve desalocar o intervalo.
&
Um endereço físico e um tamanho em bytes.
&
Não informado.
\\
\hline
EXISTS
&
Informar se o intervalo está alocado. Outros estados possíveis do intervalo são: mapeado e não mapeado. Estes estados são úteis caso o mecanismo thin provisioning\citep{ibmthin} seja utilizado.
&
Um endereço físico e um tamanho em bytes.
&
Volume PM referente o intervalo informado.
\\
\hline

\end{tabular}
\end{center}
\label{table:nvm-pm-volume-actions}
\end{table}

\begin{table}[h]
\begin{center}
\caption{Atributos do modo de acesso NVM.PM.VOLUME}
\begin{tabular}{|p{3cm}|p{5.3cm}|p{3.3cm}|}
\hline
Atributo & Propósito & Tipo\\
\hline
VOLUME
{\_}SIZE
&
A soma dos tamanhos de todos intervalos referentes um volume PM.
&
inteiro
\\
\hline
INTERRUPTED
{\_}STORE
{\_}ATOMICITY
&
Indica se o dispositivo suporta atomicidade na operação store em caso de queda de energia.
&
booleano
\\
\hline
FUNDAMENTAL
{\_}ERROR
{\_}RANGE
&
Número de bytes que podem tornar-se indisponíveis em virtude de um erro no dispositivo. Dados podem ser organizados em função deste valor de modo que, no erro de um intervalo, apenas uma informação seja perdida.
&
inteiro
\\
\hline
FUNDAMENTAL
{\_}ERROR
{\_}RANGE
{\_}OFFSET
&
Deslocamento para alinhar os intervalos informados no atributo anterior.
&
inteiro
\\
\hline
DISCARD
{\_}IF{\_}YOU{\_}
CAN{\_}CAPABLE
&
Indica se o dispositivo suporta a ação DISCARD{\_}IF{\_}YOU{\_}CAN.
&
booleano
\\
\hline
DISCARD
{\_}IMMEDIATELY
{\_}CAPABLE
&
Indica se o dispositivo suporta a ação DISCARD{\_}IMMEDIATELY.
&
booleano
\\
\hline
DISCARD
{\_}IMMEDIATELY
{\_}RETURNS
&
Valor retornado para operações de leitura de endereços que foram descartados e ainda não foram sobrescritos.
&
Não definido.
\\
\hline
EXISTS
{\_}CAPABLE
&
Indica se o dispositivo suporta a ação EXISTS.
&
booleano
\\
\hline
\end{tabular}
\end{center}
\label{table:nvm-pm-volume-attr}
\end{table}

Um exemplo de uso deste modo de acesso é a configuração inicial que um sistema de arquivos que irá manipular arquivos em Persistent Memory deve fazer. Partindo do princípio que um volume PM já foi pré-definido pelo administrador do sistema, o sistema de arquivos que irá manipular arquivos neste volume deve adquirir, através do modo de acesso NVM.PM.VOLUME, informações referentes a este volume. A interface de programação fornecida pelo componente do sistema operacional responsável pela implementação do NVM.PM.VOLUME deverá ser acessada e a ação GET{\_}RANGESET acionada. Para acionar esta ação, o sistema de arquivos precisará ter uma referência a este volume, e como esta especificação está preocupada apenas com semântica mas não com sintaxe, o formato desta referência não é descrito. O sistema de arquivos recebe as informações necessárias para acessar a Persistent Memory e faz os procedimentos administrativos que o permitirão ficar utilizável pelo sistema.  A Tabela \ref{table:nvm-pm-volume-usecase} apresenta a definição deste caso de uso.

\begin{table}[h]
\begin{center}
\caption{Caso de uso Passos iniciais de um sistema de arquivos para uso de Persistent Memory}
\begin{tabular}{|p{4cm}|p{8cm}|}
\hline
Caso de uso & Passos iniciais de um sistema de arquivos para uso de Persistent Memory\\
\hline
Ator
&
Sistema de arquivos que pretende utilizar Persistent Memory.
\\
\hline
Pré-condições
&
\begin{itemize}
  \item O administrador criou o volume PM.
  \item O administrador instalou todo software necessário para utilização do sistema de arquivos.
\end{itemize}
\\
\hline
Entrada
&
\begin{itemize}
  \item Uma referência ao volume PM.
  \item O nome do sistema de arquivos.
\end{itemize}
\\
\hline
Fluxo de execução
&
\begin{enumerate}
  \item O sistema de arquivos invoca a função get{\_}rangeset provida pelo NVM.PM.VOLUME, informando a referência do volume PM.
  \item O sistema de arquivos utiliza as informações de intervalo de endereços da Persistent Memory para determinar o deslocamento que dividirá a área de metadados do sistema de arquivos da área de dados. Escreve as informações necessárias na área de metadados.
  \item O sistema de arquivos informa na área de metadados que o sistema de arquivos está montado naquele volume
	\begin{enumerate}
		\item Se o intervalo referente ao volume requer VIRTUAL{\_}ADDRESS{\_}SYNC ou PHYSICAL{\_}ADDRESS{\_}SYNC, o sistema de arquivos invoca a ação de sincronização apropriada.
	\end{enumerate}
\end{enumerate}
\\
\hline
Pós-condições
&
O sistema de arquivos está disponível para uso.
\\
\hline
\end{tabular}
\end{center}
\label{table:nvm-pm-volume-usecase}
\end{table}


\section{NVM.PM.FILE}
\label{sec:nvm_pm_file}

O modo de acesso NVM.PM.FILE define o comportamento da interface de programação a ser utilizada pelos programas que executam em modo usuário. Esta interface de programação é implementada por um sistema de arquivos que tem conhecimento sobre o hardware que persistirá os dados e por consequência tem a capacidade de distinguir quando um arquivo está armazenado em uma Persistent Memory. Assim, quando o programa em modo usuário utilizar as chamadas de sistema read() e write(), o sistema de arquivos não utilizará a infraestrutura de cache através de paginação, conforme discutido na Seção \ref{sec:no_page_cache}. 
Este modo é totalmente compatível com os programas existentes, a presença ou não de uma Persistent Memory é transparente. Apesar disso, é interessante que o programador saiba quando o sistema de arquivos provê acesso a uma Persistent Memory, pois poderá otimizar seu código dando preferência a utilização da chamada de sistema mmap(), mapeando um intervalo do espaço de enderaçamento do processo diretamente a endereços da Persistent Memory, através da MMU, como visto na Figura  \ref{fig:nvm-pm-file}. A função mmap() é modelada na ação NVM.PM.FILE.MAP. A otimização poderia ser realizada pelo compilador, na fase de geração de código intermediário, trocando instruções read() e write() pela instrução mmap(), porém isto não é recomendado pois a decisão de armazenamento de um arquivo em Persistent Memory ou outro tipo de memória secundária não é conhecida em tempo de compilação e pode sofrer alterações ao longo do tempo. Além disso, esta alteração implicaria em abrir mão da infraestrutura de tolerância a falhas provida pelo sistema de arquivos. O modelo informa que o sistema de arquivos deve ser capaz de operar com arquivos em Persistent Memory e dispositivos orientados a bloco simultaneamente, porém não define como o sistema de arquivos deve diferenciar arquivos que estão armazenados em Persistent Memory. Vale destacar que o componente responsável pela implementação deste modo de acesso não tem autonomia para selecionar que volume PM pode acessar, mas deve solicitá-lo ao componente que implementa o modo de acesso NVM.PM.VOLUME. A Figura  \ref{fig:nvm-pm-file} apresenta a interface de acesso, destacada em vermelho. O sistema de arquivos é responsável por sua implementação, disponibilizando-a para os programas em modo usuário. O programa poderá optar em utilizar as chamadas de sistema nativas de sistemas de arquivos ou executar diretamente operações de load/store na Persistent Memory, através de mapeamento na MMU criado pela ação NVM.PM.FILE.MAP.

A Tabela \ref{table:nvm-pm-file-actions}  apresenta todas ações disponibilizadas por este modo de acesso e a Tabela \ref{table:nvm-pm-file-attr} todos os atributos.



\begin{figure}[h]
\centering
\includegraphics[scale=0.6]
	{images/nvm-pm-file.eps}
\caption{Exemplo de uso do modo de acesso NVM.PM.FILE}
\label{fig:nvm-pm-file}
\end{figure}


\begin{table}[h]
\begin{center}
\caption{Ações do modo de acesso NVM.PM.FILE}
\begin{tabular}{|p{2.0cm}|p{4cm}|p{4cm}|p{4cm}|}
\hline
Ação & Propósito & Entrada & Saída\\
\hline
Ações nativas de sistemas de arquivos.
&
Suportar operações tradicionais de sistemas de arquivos.
&
Fornecida pela implementação nativa da função de mapeamento oferecida pelo sistema operacional.
&
Fornecida pela implementação nativa da função de mapeamento oferecida pelo sistema operacional.
\\
\hline
MAP
&
Adiciona um subconjunto de um arquivo em Persistent Memory para o espaço de endereçamento do usuário.
&
Fornecida pela implementação nativa da função de mapeamento oferecida pelo sistema operacional, por exemplo mmap().
&
Fornecida pela implementação nativa da função de mapeamento oferecida pelo sistema operacional, por exemplo mmap().
\\
\hline
SYNC
&
Sincronizar conteúdo da memória cache para Persistent Memory.
&
Fornecida pela implementação nativa da função de mapeamento oferecida pelo sistema operacional, por exemplo msync().
&
Fornecida pela implementação nativa da função de sincronização oferecida pelo sistema operacional, por exemplo msync().
\\
\hline
OPTIMIZED
{\_}FLUSH
&
Mesmo propósito da ação SYNC, porém com a intenção de permitir otimizações adicionais, por exemplo permitindo sincronização de múltiplos intervalos de memória.
&
Idêntica a fornecida pela ação SYNC.
&
Idêntica a fornecida pela ação SYNC.
\\
\hline
GET
{\_}ERROR
{\_}EVENT
{\_}INFO
&
Prover informações que permitam que a aplicação tome decisão de recuperação de dados.
&
Assume-se que esta ação receberá a saída gerada por um evento controlado.
&
1 = Indica que a aplicação pode retomar sua execução a partir deste ponto de interrupção.

2 = Indica erro que permite que controle pela aplicação.

3 = Houve perda de dados.
\\
\hline
OPTIMIZED
{\_}FLUSH
{\_}AND
{\_}VERIFY
&
Mesmo propósito da ação OPTMIZED{\_}FLUSH, porém a sincronização do dado é verificada, fornecendo maior segurança quanto a persistência dos dados.
&
Idêntica a fornecida pela ação SYNC.
&
Idêntica a fornecida pela ação SYNC.
\\
\hline

\end{tabular}
\end{center}
\label{table:nvm-pm-file-actions}
\end{table}



\begin{table}
\begin{center}
\caption{Atributos do modo de acesso NVM.PM.FILE}
\begin{tabular}{|p{5cm}|p{6cm}|p{3cm}|}
\hline
Atributo & Propósito & Tipo\\
\hline
MAP
{\_}COPY
{\_}ON{\_}WRITE
{\_}CAPABLE
&
Indica que a opção MAP{\_}COPY{\_}ON{\_}WRITE é suportada pela ação MAP.
&
booleano
\\
\hline
INTERRUPTED
{\_}STORE
{\_}ATOMICITY
&
Indica se o dispositivo suporta atomicidade na operação store em caso de queda de energia.
&
booleano
\\
\hline
FUNDAMENTAL
{\_}ERROR
{\_}RANGE
&
Número de bytes que podem tornar-se indisponíveis em virtude de um erro no dispositivo. Dados podem ser organizados em função deste valor de modo que, no erro de um intervalo, apenas uma informação seja perdida.
&
inteiro
\\
\hline
FUNDAMENTAL
{\_}ERROR
{\_}RANGE
{\_}OFFSET
&
Deslocamento para alinhar os intervalos informados no atributo anterior.
&
inteiro
\\
\hline
OPTIMIZED
{\_}FLUSH
{\_}CAPABLE
&
Indica se o dispositivo suporta a ação OPTIMIZED{\_}FLUSH.
&
booleano
\\
\hline
OPTIMIZED
{\_}FLUSH
{\_}AND
{\_}VERIFY
{\_}CAPABLE
&
Indica se o dispositivo suporta a ação OPTIMIZED{\_}FLUSH{\_}AND{\_}VERIFY.
&
booleano
\\
\hline
\end{tabular}
\end{center}
\label{table:nvm-pm-file-attr}
\end{table}

Um exemplo de uso deste modo de acesso é  o caso de uso descrito na Tabela \ref{table:nvm-pm-file-usecase}. Na implementação da função mmap() em um sistema de arquivos tradicional, uma área da memória principal é alocada para manipulação dos dados armazenados no arquivo. Em um sistema de arquivos que conhece e utiliza uma Persistent Memory, a implementação da função mmap() não precisa alocar uma área na memória principal, mas prover mapeamento entre o espaço de endereço do usuário e o intervalo de endereços físicos que representa o arquivo mapeado. A função mmap() é modelada pela ação NVM.PM.FILE.MAP. A sincronização dos dados é realizada pela função msync() e modelada pela ação NVM.PM.FILE.SYNC ou pelas ações especializadas NVM.PM.FILE.OPTIMIZED{\_}FLUSH e NVM.PM.FILE.OPTIMIZED{\_}FLUSH{\_}AND{\_}VERIFY.


\begin{table}[h]
\begin{center}
\caption{Caso de uso Atualizar um registro de um arquivo armazenado em Persistent Memory}
\begin{tabular}{|p{4cm}|p{8cm}|}
\hline
Caso de uso & Atualizar um registro de um arquivo armazenado em Persistent Memory
\\
\hline
Ator
&
Uma aplicação que acessa um arquivo disponível, através de um sistema de arquivos, em uma Persistent Memory.
\\
\hline
Pré-condições
&
\begin{itemize}
  \item O administrador do sistema já instalou um sistema de arquivos na Persistent Memory e criou o arquivo que será atualizado
  \item A aplicação possui uma referência a este arquivo e já o populou com dados.
  \item O arquivo não está em uso no início deste caso de uso.
\end{itemize}
\\
\hline
Entrada
&
\begin{itemize}
  \item O conteúdo a ser atualizado no registro do arquivo.
  \item O deslocamento do registro no arquivo.
\end{itemize}
\\
\hline
Fluxo de execução
&
\begin{enumerate}
  \item A aplicação utiliza a chamada de sistema nativa open().
  \item A aplicação utiliza a ação MAP, passando o descritor do arquivo retornado pelo passo anterior.
  \item A aplicação acessa o endereço de memória mapeado + o deslocamento referente posição do registro e atualiza o conteúdo do registro.
  \item A aplicação utiliza a ação SYNC para garantir a persistência dos dados.
  \item A aplicação utiliza as chamadas de sistema nativas unmap() e close().
\end{enumerate}
\\
\hline
Pós-condições
&
O registro está atualizado.
\\
\hline
\end{tabular}
\end{center}
\label{table:nvm-pm-file-usecase}
\end{table}
