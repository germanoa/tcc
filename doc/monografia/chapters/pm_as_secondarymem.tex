\chapter{Persistent Memory como memória secundária} % 1 to 1-1/2 pages for section
\label{chap:pm_as_secondarymem}

Persistent Memory (PM)\footnote {http://www.snia.org/education/dictionary/p acessado em 10 de novembro de 2014}, também conhecida como Storage Class Memory (SCM) \citep{mnemosyne}, é o nome dado para a classe de memórias não voláteis (NVM) endereçada a byte, e por isso acessível diretamente pelo processador. Programas de computador precisam de memória para manipular dados e, eventualmente, armazená-los. Apesar de uma PM possuir todas características necessárias para ser utilizada diretamente como memória principal de um computador, só será interessante sua utilização como memória principal se possuir relação desempenho e custo melhor que memórias voláteis. Em contrapartida, sua velocidade de tempo de acesso aproximadamente 1000 vezes mais rápido que memórias NAND a tornam uma promissora memória secundária. A Tabela \ref{table:tech_pm_table} mostra exemplos de tecnologias utilizadas para construção de Persistent Memory. Persistent Memory do tipo NVDIMM já é comercializada\footnote{http://www.vikingtechnology.com/nvdimm-technology acessado em 10 de novembro de 2014}, chegando a capacidade de até 8GB. Acredita-se que em 2020 Persistent Memory já esteja competindo efetivamente com discos rígidos.\citep{SCM_IBMJRD}.

\begin{table}[h]
\begin{center}
\caption{Tecnologias utilizadas na construção de Persistent Memory}
\begin{tabular}{|p{5.5cm}|p{6.7cm}|}
\hline
Tecnologia & Características de armazenamento\\
\hline
Phase Change Memory (PCM)
&
Energia (calor) converte material entre as fases cristalina (condutiva) e amorfa (resistiva).
\\
\hline
Magnetic Tunnel Junction (MTJ)
&
Comutação de camada resistiva magnética por rotação polarizada de elétrons.\\
\hline
Electrochemical Cells (ECM)
&
Formação/dissolução de nano-bridge por eletroquímica.\\
\hline
Binary Oxide Filament Cells
&
Formação de filamento reversível por Oxidação-Redução.\\
\hline
Interfacial Switching
&
Oxygen vacancy drift diffusion induced barrier modulation\\
\hline
NVDIMM
&
Memória hibrida DRAM e NAND, com um capacitor que, ao detectar queda de energia, armazena os dados da DRAM na memória NAND.\\
\hline
\end{tabular}
\end{center}
\label{table:tech_pm_table}
\end{table}


\section{Memória secundária}
As atuais tecnologias desenvolvidas para armazenamento de dados possuem duas características principais que estão inversamente relacionadas: capacidade e desempenho. Programas que demandam desempenho crítico, como esta proposta de bancos de dados In-memory \citep{Lahiri_oracletimesten}, precisam se adaptar para utilização de dados persistidos em memórias não voláteis. Além disso, o crescimento da necessidade de armazenamento de informações já não é exclusividade de corporações do porte do Facebook, com demanda próxima de 60 terabytes por semana \citep{Beaver10findinga}, mas realidade de toda instituição que gera informação.

Para disponibilizar memórias secundárias com maior capacidade para os programas é necessário a abstração destes dispositivos em uma entidade lógica, denominada volume. Na área de armazenamento de dados, volume pode ser definido de diversos modos. Para este trabalho, utilizaremos a definição de volume presente no manual de administração do banco de dados Oracle, que o define como uma agregação de partes de um ou mais dispositivos de armazenamento \citep{oracle:man11g}. Uma das técnicas para fornecer volumes são os sistemas  de armazenamento centralizado. Estes sistemas são computadores com barramentos de alta densidade de memórias secundárias de diversos tipos, tais como discos SSD e discos SATA. Analogamente a técnica de memória virtual utilizada em sistemas operacionais, o sistema de armazenamento possui um software que cria discos virtuais e os apresenta aos computadores conectados a ele, ocultando a organização física dos blocos de dados. Os computadores se conectam a este sistema através de uma rede e se comunicam através de protocolos específicos, por exemplo o FCP (Fibre Channel Protocol)\citep{ibmsanconcepts}, que transporta comandos SCSI de entrada e saída. Para entregar capacidade minimizando a perda de desempenho, os blocos contínuos do disco virtual são mapeados para blocos físicos espalhados por diversos tipos de dispositivo de armazenamento, priorizando os blocos mais acessados em dispositivos com menor tempo de acesso  \citep{03creatingtiered}.

Outra abordagem conhecida para disponibilizar memórias secundárias maiores para os programas é o uso de arquiteturas com sistemas de arquivos distribuídos, como o Google File System \citep{Ghemawat03thegoogle}. Tais arquiteturas visam escalabilidade, eficiência e resiliência para o armazenamento e acesso aos dados, porém exigem alta complexidade no projeto e implementação.

Ambas propostas permitem aumento de capacidade de armazenamento mas apenas minimizam a perda de desempenho.

\subsection{Dispositivo orientado a bloco e orientado a byte (caracter)}
Bloco é uma sequência de bytes de tamanho máximo fixo. Denomina-se dispositivo orientado a bloco aquele que se comunica com o sistema operacional através de blocos de dados \citep{carissimiSO}. O sistema de arquivos é quem define o tamanho do bloco e esta definição deve considerar características do dispositivo físico. Por exemplo, discos rígidos(HDD) endereçam os dados em setores de 512 bytes. Discos SSD endereçam os dados em páginas de 4 Kbytes \citep{Agrawal_designtradeoffs}. É uma boa prática que o sistema de arquivos defina um bloco de tamanho múltiplo ao utilizado pelo dispositivo físico. Ainda, a definição deste tamanho tem relação direta com desempenho e fragmentação interna. Se o programa que manipulará arquivos neste sistema de arquivos trabalha com arquivos grandes, a utilização de um bloco grande permitirá menos sobrecarga de cabeçalhos dos protocolos de transporte dos dados e consequentemente maior desempenho. A fragmentação interna ocorrerá caso o programa manipule arquivos menores que o bloco. O tamanho padrão para bloco dos principais sistemas de arquivo é 4 Kbytes.

Dispositivos orientados a byte (caracter), por sua vez, se comunicam com granularidade de byte. Por esta característica, o endereçamento do dado nestes dispositivos é o mesmo utilizado para endereçar dipositivos de memória principal, ou seja, os dados são endereçados byte a byte. Esta abordagem está mais próxima da estrutura dos programas, evitando técnicas de transformação de estruturas de dados para noção de blocos. Por exemplo,com endereçamento a byte a serialização e deserialização de estruturas de dados referenciadas não é mais necessária.

Vale salientar que, em virtude das arquiteturas dos processadores, os sistemas de arquivos também manipulam os dados com operações load/store, através de buffers armazenados na memória principal \citep{man_buffercache}. O sistema operacional e a memória secundária precisam se comunicar de modo que os bytes solicitados sejam armazenados nestes buffers. No próximo capítulo mais detalhes desta comunicação serão abordados.

%No que tange a desempenho, esta camada de abstração de software não é gargalo para os atuais dispositivos orientados a bloco. Todavia, para Persistent Memory, esta camada de abstração torna-se evidente sobrecarga. A Figura 2.1 apresenta gráfico de avaliação gerado pela Intel que mostra que, em uma operação de leitura, a latência proporcionada pelo software representa aproximadamente 50%, enquanto que nos dispositivos com memória NAND esta latência representa menos de 15%.


\section{Persistent Memory como memória secundária}
A abordagem trivial para a utilização de dispositivos de memória secundária é disponi- bilizá-la para os programas através de um sistema de arquivos. O sistema de arquivos abstrai a interface de acesso dos dispositivos e fornece uma interface padrão. Através das funções read() e write(), os programas podem ler e escrever dados na memória persistente, independente de que dispositivo físico está sendo gerenciado pelo sistema de arquivos. Estas funções utilizam chamadas de sistema de mesmo nome das respectivas funções, padronizadas entre todos sistemas de arquivos, permitindo assim portabilidade de programas não apenas entre dispositivos físicos de armazenamento mas também entre sistemas de arquivos.

Persistent Memory é um dispositivo de armazenamento e também pode ser ocultada através de um sistema de arquivos. Apesar de ser endereçada a byte, um sistema de arquivos pode se adaptar a esta interface de acesso e fornecer as mesmas chamadas de sistema. Propostas de sistemas de arquivos exclusivos para Persistent Memory foram desenvolvidos, por exemplo pmfs\footnote{https://github.com/linux-pmfs/pmfs acessado em 22 de novembro de 2014} e pramfs\footnote{http://pramfs.sourceforge.net/tech.html acessado em 22 de novembro de 2014}.  Todavia, estes projetos não estão sendo atualizados e os novos esforços estão na compatiblização dos sistemas de arquivos existentes com Persistent Memory. Um patch para o ext4\footnote{http://lwn.net/Articles/609652/ acessado em 22 de novembro de 2014} já foi produzido.

Esta abordagem permite compatibilidade com os programas existentes, porém esta compatibilidade implica na utilização das chamadas de sistemas de sistemas de arquivos, que não foram projetadas para operação direta com dados em memória. Por exemplo, na atual semântica, a função read() lê os dados de um arquivo para um buffer na memória principal, o que não seria necessário caso o dado estivesse originalmente em uma Persistent Memory.

No próximo capítulo, mais detalhes sobre o uso de sistema de arquivos serão discutidos, destacando problemáticas e soluções na adaptação destes sistemas para o uso de Persistent Memory.


\section{Observação sobre persistência}
\label{sec:pm_persistencia}

Do ponto de vista de uma operação de escrita, entre a geração do dado no processador e o armazenamento na memória secundária, existe um sistema de cache hierárquico. Quão mais próxima do processador, mais rápida é a memória. Por exemplo, a arquitetura Nehalem \citep{cache_nehalem} de processadores da Intel foi avaliada e apresentou latência de 4, 10 e 38 ciclos de clock para as memórias cache L1, L2 e L3, respectivamente. Quanto ao tempo de acesso, a cache L3 pôde ser acessada em 13 ns enquanto que a memória principal em 65 ns. Vale salientar que o tempo de acesso as memórias cache dependem exclusivamente da arquitetura do processador, enquanto que o acesso a memória principal depende também do barramento que a conecta ao processador. Este sistema é inclusivo, ou seja, dados presentes em uma cache mais próxima do processador devem estar presentes em todas outras caches. Quando um dado presente em uma cache é alterado, ele passa para o estado Modificado e algum algoritmo precisa ser executado para manter coerência com as demais caches e com a memória principal. Por fim, o sistema operacional tem a responsabilidade de manter a coerência do dado entre a memória principal e a memória secundária. Todo este processo é realizado com o objetivo de proporcionar melhor desempenho na operação do computador, porém ele dificulta na garantia da durabilidade do dado, ou seja, na garantia que o dado esteja persistido na memória secundária, não volátil.

Algoritmos de escrita em cache são utilizados para manter a coerência entre as caches, e os dois mais conhecidos são o write-back e write-through. No algoritmo write-through, o dado é copiado por toda hierarquia de caches até chegar a memória principal, liberando então o processador para a próxima instrução. Neste algoritmo, utiliza-se a política de atualizar imediatamente o dado na memória secundária, tornando a cache efetiva apenas para as operações de leitura. No algoritmo write-back, o dado é copiado apenas para a primeira cache onde está presente, acelerando a liberação do processador para a próxima instrução. O dado passa ao estado Modificado e será copiado para o restante da hierarquia até chegar a memória principal na próxima operação de leitura que o requisitar. Por este motivo, esta é uma técnica de escrita assíncrona, porém largamente utilizada pela considerável melhora de desempenho. Neste algoritmo, a política utilizada é de postergar ao máximo a atualização do disco \citep{carissimiSO}. Sistemas operacionais possuem chamadas de sistema de sincronismo que permitem ao administrador forçar a coerência entre as caches e memória principal. Pesquisas apresentam propostas que podem eliminar a necessidade de instruções de sincronismo. Uma delas é a arquitetura Kiln \citep{kiln_arch}, na qual a cache dos processadores também é persistente.
    
Pelos motivos apresentados, apesar do nome Persistent Memory, a arquitetura existente não permite que uma operação de escrita executada no processador seja suficiente para garantirmos a persistência do dado na memória secundária. Operações de sincronismo são necessárias. Quanto à coerência entre a memória principal e a memória secundária, de responsabilidade do sistema operacional, otimizações serão apresentadas no próximo capítulo.    
