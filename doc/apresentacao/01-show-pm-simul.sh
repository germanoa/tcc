#!/bin/bash

clear

TCC=/home/gandersson/tcc

echo "cat /proc/cmdline"
cat /proc/cmdline

echo "cat /proc/iomem |grep 80000000"
cat /proc/iomem |grep 80000000

for i in `cat /proc/iomem |grep reserved |cut -d"-" -f1 |sed -e 's/ */0x/'`; do bc <<< `printf '%d\n' $i`/1048576 |grep 2048 ;done

echo "cat $TCC/src/pmdrv/pmdrv.h |grep '_ADDR'"
cat $TCC/src/pmdrv/pmdrv.h |grep '_ADDR'
