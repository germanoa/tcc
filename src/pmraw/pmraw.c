#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/device.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

#include <linux/errno.h>
#include <linux/mm.h>

#include </tcc/pmraw/pmraw.h>

MODULE_LICENSE("Dual BSD/GPL");

int major[PM_MAXVOLUMES],minor[PM_MAXVOLUMES];
int volumes=0;

static struct class *pmclass = NULL;

static int pmraw_mmap(struct file *file, struct vm_area_struct *vma);
static int pmraw_mmap(struct file *file, struct vm_area_struct *vma)
{
	unsigned long off,physical,vsize,psize,id;
	struct pmvolume_t pm;

	id = MINOR(file->f_dentry->d_inode->i_rdev);

	// interface NVM.PM.VOLUME(pmdrv)
	pm = get_rangeset(id);

	//validar pm (if pm==null)
	
	off = vma->vm_pgoff >> PAGE_SHIFT;
	physical = pm.phys_addr + off;
	vsize = vma->vm_end - vma->vm_start;
	psize = pm.size - off;
	

	if (vsize > psize) {
		printk(KERN_ALERT "PMRAW: INVALID SIZE.\n");
	  return -EINVAL;
	}
	remap_pfn_range(vma, vma->vm_start, physical >> PAGE_SHIFT,
		vsize, vma->vm_page_prot);
	printk(KERN_INFO "PMRAW%lu:\n\tphysical:%lX\n\tvirtual:%lX\n\toffset:%lu\n",
		id,physical>>PAGE_SHIFT,vma->vm_start,vsize);

	return 0;
}

static const struct file_operations pmraw_fops = {
	.owner = THIS_MODULE,
	.mmap = pmraw_mmap,
};

/* init string with 0's */
void static inline _init_str(char *s,int size)
{
  int i;
  for (i=0;i<size;++i) {
    s[i]=0;
  }
}

void static inline _init_int(int *s,int size)
{
  int i;
  for (i=0;i<size;++i) {
    s[i]=0;
  }
}

/* return pos of char from string */
int static inline _search_char(const char *s, char c, int size)
{
  int i,ret=0;
  for (i=0;i<size;++i) {
    if (s[i]==c) {
      ret=i;
      break;
    }   
  }
  return ret;
}

int static inline _exists_pmraw(int id)
{
  int i,ret=0;
  for (i=0;i<PM_MAXVOLUMES;++i) {
    if (minor[i]==id) {
      ret=1;
      break;
    }   
  }
  return ret;
}

int _pmraw_insert(char *strpm,unsigned long id) {
	int ret=1;
	struct device *pmdevice = NULL;

	minor[volumes] = id;
	major[volumes] = register_chrdev(UNNAMED_MAJOR,strpm,&pmraw_fops);
	pmdevice = device_create(pmclass,NULL,
		MKDEV(major[volumes],minor[volumes]),NULL,strpm);
	volumes++;
	if(IS_ERR(pmdevice)) {
		ret=0;
	}
	return ret;
}

int pmraw_proc_write(struct file *file, const char *buffer, unsigned long count,
       void *data)
{
  unsigned long id,x,pos;
  char pmraw_proc_buffer[PMRAW_PROC_MAXSIZE];
  unsigned long pmraw_proc_buffer_size = 0;
	int ret;

	char strid[PM_ID_MAXSIZE];
	char strpm[PMRAW_NAME_SIZE];

	_init_str((void*)pmraw_proc_buffer,PMRAW_PROC_MAXSIZE);
	_init_str((void*)strid,PM_ID_MAXSIZE);
	_init_str((void*)strpm,PMRAW_NAME_SIZE);
	strncpy(strpm,PMRAW_CLASS_NAME,6);

  pmraw_proc_buffer_size = count;
  if (pmraw_proc_buffer_size > PMRAW_PROC_MAXSIZE ) {
    pmraw_proc_buffer_size = PMRAW_PROC_MAXSIZE;
  }
	ret = pmraw_proc_buffer_size;
  
  /* write data to the buffer */
  if ( copy_from_user(pmraw_proc_buffer, buffer, pmraw_proc_buffer_size) ) {
  //if ( copy_from_user(pmraw_proc_buffer, buffer, sizeof(pmraw_proc_buffer)) ) {
    return -EFAULT;
  }

	pos = _search_char(pmraw_proc_buffer,'.',pmraw_proc_buffer_size);
	strncpy(strid,pmraw_proc_buffer,pos);

	x = strict_strtoul(strid,10,&id);
  if (x) {
    printk(KERN_ALERT "PMRAW: kstrtoul returns %lu\n",x);
    printk(KERN_ALERT "PMRAW: format error\n");
  }

	// interface NVM.PM.VOLUME(pmdrv)
	if (!exists_pmvolume(id)) {
    printk(KERN_ALERT "PMRAW: Volume does not exist.\n");
		ret = PMRAW_ERROR_NOTVOL;
	} else if (_exists_pmraw(id)) {
    printk(KERN_ALERT "PMRAW: /dev/pmraw%lu already exists..\n",id);
		ret = PMRAW_ERROR_IDEXIST;
	} else { /* create pmraw device */
		strncat(strpm,strid,PM_ID_MAXSIZE);

		if(!_pmraw_insert(strpm,id)) {
			printk(KERN_ALERT "PMRAW: pmdevice error.\n");
			ret = PMRAW_ERROR_DEV;
		}
	}
  return ret;
}

void pmraw_init_procfs(void)
{
  static struct proc_dir_entry *pmraw_proc;

  //pmraw_proc = create_proc_entry(PMRAW_PROC_ENTRY, 0644, pm_proc_root);
  pmraw_proc = create_proc_entry(PMRAW_PROC_ENTRY, 0644, NULL);
  
  if (pmraw_proc == NULL) {
    printk(KERN_ALERT "Error: Could not initialize /proc/%s/%s\n",
      PMDRV_PROC_DIR,PMRAW_PROC_ENTRY);
  }

  pmraw_proc->write_proc = pmraw_proc_write;
  pmraw_proc->mode    = S_IFREG | S_IRUGO;

  printk(KERN_INFO "/proc/%s/%s created\n", PMDRV_PROC_DIR,PMRAW_PROC_ENTRY); 
}

static int pmraw_init(void)
{
	printk(KERN_INFO "init pmraw.\n");

	_init_int(minor,PM_MAXVOLUMES);
	_init_int(major,PM_MAXVOLUMES);
	volumes = 0;

	pmclass = class_create(THIS_MODULE,PMRAW_CLASS_NAME);
	if(IS_ERR(pmclass)) {
		printk(KERN_ALERT "PMRAW: PMCLASS error.\n");
	}

	pmraw_init_procfs();

	return 0;
}

void pmraw_exit_procfs(void)
{
  //remove_proc_entry(PMRAW_PROC_ENTRY, pm_proc_root);
  remove_proc_entry(PMRAW_PROC_ENTRY, NULL);
}


static void pmraw_exit(void)
{
	char strminor[PM_ID_MAXSIZE];
	char strpm[PMRAW_NAME_SIZE] = PMRAW_CLASS_NAME;
	int i;

  for(i=0;i<volumes;++i) {
		_init_str((void*)strpm,PMRAW_NAME_SIZE);
		_init_str((void*)strminor,PMRAW_NAME_SIZE);
		strncpy(strpm,PMRAW_CLASS_NAME,6);

		sprintf(strminor,"%d",minor[i]);
		strncat(strpm,strminor,PM_ID_MAXSIZE);

		device_destroy(pmclass, MKDEV(major[i], minor[i]));
		unregister_chrdev(major[i],strpm);
		printk(KERN_ALERT "PMRAW: unregistering %s.\n",strpm);
	}

	class_unregister(pmclass);
	class_destroy(pmclass);
	
	pmraw_exit_procfs();
	

	printk(KERN_INFO "exit pmraw.\n");
}

module_init(pmraw_init);
module_exit(pmraw_exit);
