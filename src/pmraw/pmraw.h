#ifndef _PMRAW_H
#define _PMRAW_H

#include </tcc/pmdrv/pmdrv.h>

/* PMRAW macros */
#define PMRAW_CLASS_NAME "pmraw"
#define PMRAW_NAME_SIZE 10

/* PMRAW PROCFS macros */
#define PMRAW_PROC_ENTRY "pmraw"
#define PMRAW_PROC_MAXSIZE 1000

/* PMRAW ERROR macros */
#define PMRAW_ERROR_NOTVOL -100
#define PMRAW_ERROR_IDEXIST -101
#define PMRAW_ERROR_DEV -102

#endif /* _PMRAW_H */
