#include <linux/init.h>
#include <linux/module.h>
#include <linux/vmalloc.h> 
#include <asm/io.h>
#include <linux/mman.h>

MODULE_LICENSE("Dual BSD/GPL");


int *x;

int get_rangeset(void)
{
	//int map_pm = MAP_PM;
	//int map_pm = MAP_PMM;
	//printk(KERN_ALERT "MAP_PM: %X - %d.\n",map_pm,map_pm);
	printk(KERN_ALERT "rocky getrangeset.\n");
	printk(KERN_ALERT "rocky: %x - %d + 1 = %d.\n",(unsigned int) x,(int)x,++*x);
	
	return 0;
}
EXPORT_SYMBOL(get_rangeset);


static int rocky_init(void)
{
	printk(KERN_ALERT "init rocky.\n");


	// avoiding page cache?? see XIP
	//x = (void __iomem*) ioremap_nocache(0x80000000,20);
	x = (void __iomem*) ioremap_nocache(0x8000000,20);

	get_rangeset();
	
	return 0;
}

static void rocky_exit(void)
{
	printk(KERN_ALERT "exit rocky.\n");
}

module_init(rocky_init);
module_exit(rocky_exit);
