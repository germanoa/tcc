#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>

#define RAW_DATA_OFFSET 0x80000000
#define RAW_DATA_SIZE 0x20000000


int main(int argc, char *argv[])
{

	int fd;
	int *pm;

	printf("antes de abrir /dev/pm\n");

	if ((fd = open("/dev/pmraw_1", O_RDWR) ) < 0) {
		printf("can't open /dev/pm \n");
	}
	printf("depois de abrir /dev/pm\n");
	
	//if ((pm = malloc(sizeof(int)*10)) == NULL) {
	//	printf("allocation error \n");
	//}

	printf("antes do mmap\n");
	pm = (int *)mmap(NULL,sizeof(int)*8,PROT_READ|PROT_WRITE,MAP_FILE|
		MAP_SHARED|MAP_FIXED,fd,0);
	printf("depois do mmap: pm = %X\n",pm);

	if ((long)pm < 0) {
		printf("mmap error \n");
	}

	printf("Press enter to continue\n");
	char enter = 0;
	while (enter != '\r' && enter != '\n') { enter = getchar(); }
	printf("Thank you for pressing enter\n");

	pm[0] = pm[0]+1;
	printf("pm: %d\n",*pm);

	printf("Press enter to continue\n");
	enter = 0;
	while (enter != '\r' && enter != '\n') { enter = getchar(); }
	printf("Thank you for pressing enter\n");

}
