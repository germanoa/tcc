	.file	"file_mmap.c"
	.section	.rodata
.LC0:
	.string	"/tmp/f"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$48, %esp
	movl	$2, 4(%esp)
	movl	$.LC0, (%esp)
	call	open
	movl	%eax, 40(%esp)
	movl	$0, 20(%esp)
	movl	40(%esp), %eax
	movl	%eax, 16(%esp)
	movl	$0, 12(%esp)
	movl	$3, 8(%esp)
	movl	$400, 4(%esp)
	movl	$0, (%esp)
	call	mmap
	movl	%eax, 36(%esp)
	movl	$0, 44(%esp)
	jmp	.L2
.L3:
	movl	44(%esp), %eax
	leal	0(,%eax,4), %edx
	movl	36(%esp), %eax
	addl	%eax, %edx
	movl	44(%esp), %eax
	movl	%eax, (%edx)
	addl	$1, 44(%esp)
.L2:
	cmpl	$99, 44(%esp)
	jle	.L3
	movl	$0, %eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
