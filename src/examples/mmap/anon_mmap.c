/*************************************************************************\
*                  Copyright (C) Michael Kerrisk, 2014.                   *
*                                                                         *
* This program is free software. You may use, modify, and redistribute it *
* under the terms of the GNU Affero General Public License as published   *
* by the Free Software Foundation, either version 3 or (at your option)   *
* any later version. This program is distributed without any warranty.    *
* See the file COPYING.agpl-v3 for details.                               *
\*************************************************************************/

//some cuts...

/* anon_mmap.c

   Demonstrate how to share a region of mapped memory between a parent and
   child process without having to create a mapped file, either through the
   creation of an anonymous memory mapping or through the mapping of /dev/zero.
*/
#ifdef USE_MAP_ANON
#define _BSD_SOURCE             /* Get MAP_ANONYMOUS definition */
#endif
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
//#include "tlpi_hdr.h"

int
main(int argc, char *argv[])
{
    int *addr; 

    //addr = mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE,
    addr = mmap(0, sizeof(int)*100, 0,
                MAP_ANONYMOUS, -1, 0);

    //*addr = 7;    /* Initialize integer in mapped region */
		int i;
		for (i=0;i<100;++i) {
			addr[i] = i;
		}

		return 0;
}
