#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#define SIZE 512
#define FILENAME_SIZE 256

int main(int argc, char *argv[]) {

	int ret = 0;
	int fd = 0;
	char buf[SIZE],filename[FILENAME_SIZE];

	if (argc!=2) {
		fprintf(stderr,"use: %s path/to/file\n",argv[0]);
		ret = -1000;
	} else {
		size_t length = strlen(argv[1]) + 1;
		if (length>FILENAME_SIZE) {
			fprintf(stderr,
			"path/to/file with more than %d chars\n",FILENAME_SIZE);
			ret = -1001;
		} else {
    			strncpy(filename,argv[1],length);
			fd = (int) open(filename,O_RDONLY);
			if (fd) {
				ret = read(fd,buf,SIZE);
				close(fd);
			}
		}
	}
	if (ret>0) {
		--ret;
		printf("%d\n",ret);
	} else {
		fprintf(stderr,"error %d\n",ret);
	}
	return ret;

}
