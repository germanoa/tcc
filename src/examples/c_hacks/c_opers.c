#include <stdio.h>

int main()
{
	printf("1 << 12 = %d\n",1 << 12);
	printf("1 << 11 = %d\n",1 << 12);
	printf("1 << 10 = %d\n",1 << 12);
	printf("1 << 1 = %d\n",1 << 1);
	printf("0 << 1 = %d\n",1 << 1);
	printf("~(4096-1) = %d\n",~(4096-1));	
	printf("0 & -4096 = %d\n",0 & -4096);
	printf("1 & -4096 = %d\n",1 & -4096);
	
//	int i,j;
//	for (i=0;i<100;++i) {
//		j = i*4096;
//		printf("%d & -4096 = %d\n",j,j & -4096);
//		j++;
//		printf("%d & -4096 = %d\n",j,j & -4096);
//		j=j*j;
//		printf("%d & -4096 = %d\n",j,j & -4096);
//	}

	return 0;
}
