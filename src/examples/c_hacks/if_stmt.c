#include <stdio.h>

#define PAGE_SHIFT      12
#define PAGE_SIZE       (1 << PAGE_SHIFT)
#define PAGE_MASK       (~(PAGE_SIZE-1))

int main()
{
	printf("PAGE_SIZE %d\n",PAGE_SIZE);
	printf("PAGE_MASK %d\n",PAGE_MASK);
	printf("~PAGE_MASK %d\n",~PAGE_MASK);

	if(0) { printf("dont\n"); }
	if(1) { printf("1\n"); }
	if(2) { printf("2\n"); }
	if(8192 & ~PAGE_MASK) { printf("8192 & %d\n",~PAGE_MASK); }
	if(40 & ~PAGE_MASK) { printf("40 & %d\n",~PAGE_MASK); }
	if(400003 & ~PAGE_MASK) { printf("400003 & ~PAGE_MASK\n"); }



	return 0;
}
