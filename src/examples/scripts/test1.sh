#!/bin/bash

HOME="/home/germanoa"
DIRBASE="$HOME/tcc"

#cd $DIRBASE/src/examples/rocky
#make

cd $DIRBASE/src/examples/liquid
make

cd $DIRBASE/src/examples/mmap
gcc liquid_mmap.c -o ../bin/liquid

echo "please verify if /dev/pm already exists."
echo "if not, # mknod /dev/pm c 249 0"
