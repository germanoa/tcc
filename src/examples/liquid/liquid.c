#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/major.h>

#include <linux/errno.h>
#include <linux/mm.h>

//#include <../rocky/rocky.h>

MODULE_LICENSE("Dual BSD/GPL");

#define RAW_DATA_OFFSET 0x80000000
#define RAW_DATA_SIZE 0x20000000


static int liquid_mmap(struct file *file, struct vm_area_struct *vma);
static int liquid_mmap(struct file *file, struct vm_area_struct *vma)
{
//	unsigned long addr;

/*
	printk(KERN_ALERT "PAGE_SHIFT = %d\n",PAGE_SHIFT);

	addr = 0x80000000;
	printk(KERN_ALERT "addr = %lu\n",addr);
	addr = 0x80000000 <<PAGE_SHIFT;
	printk(KERN_ALERT "addr <<PAGE_SHIFT = %lu\n",addr);
	
	printk(KERN_ALERT "vm_pgoff = %lu\n",vma->vm_pgoff);
	addr = 0x80000000 <<PAGE_SHIFT;
	printk(KERN_ALERT "vm_pgoff <<PAGE_SHIFT = %lu\n",vma->vm_pgoff<<PAGE_SHIFT);

	//addr = vma->vm_pgoff + (addr >> PAGE_SHIFT);
*/

/*
	if (io_remap_pfn_range(vma, vma->vm_start, vm->vm_pgoff,
		vma->vm_end - vma->vm_start,vma->vm_page_prot))
		return -EAGAIN;
*/
	unsigned long simple_region_start = 0x80000000;
	unsigned long simple_region_size = 0x20000000;
	

	printk(KERN_ALERT "PAGE_SHIFT = %d\n",PAGE_SHIFT);
	printk(KERN_ALERT "vma->vm_pgoff = %lu\n",vma->vm_pgoff);
	printk(KERN_ALERT "vma->vm_pgoff << PAGE_SHIFT = %lu\n",vma->vm_pgoff << PAGE_SHIFT);
	printk(KERN_ALERT "vma->vm_pgoff >> PAGE_SHIFT = %lu\n",vma->vm_pgoff >> PAGE_SHIFT);
	printk(KERN_ALERT "simple_reg_start = %lu - %X\n",simple_region_start,simple_region_start);
	printk(KERN_ALERT "simple_reg_start >> PAGE_SHIFT = %lu - %X\n",simple_region_start >> PAGE_SHIFT,simple_region_start >> PAGE_SHIFT);
	//unsigned long off = vma->vm_pgoff << PAGE_SHIFT;
	unsigned long off = vma->vm_pgoff >> PAGE_SHIFT;
	unsigned long physical = simple_region_start + off;
	printk(KERN_ALERT "phy = %lu\n",simple_region_start + off);
	unsigned long vsize = vma->vm_end - vma->vm_start;
	unsigned long psize = simple_region_size - off;
	
	if (vsize > psize)
	    return -EINVAL; /*  spans too high */
	remap_pfn_range(vma, vma->vm_start, physical >> PAGE_SHIFT, vsize, vma->vm_page_prot);

//	remap_pfn_range(vma, vma->vm_start,RAW_DATA_OFFSET >> PAGE_SHIFT, 
  //      	RAW_DATA_SIZE, PAGE_SHARED);
			
	//vma->vm_ops = &simple_remap_vm_ops;
	//simple_vma_open(vma);

	printk(KERN_ALERT "mmap exec at liquid!\n");
	return 0;
}


static const struct file_operations liquid_fops = {
	.owner = THIS_MODULE,
	.mmap = liquid_mmap,
};


static int liquid_init(void)
{
	int ret;

	printk(KERN_ALERT "init liquid.\n");

	ret = register_chrdev(UNNAMED_MAJOR, "pm",&liquid_fops);

	//x = get_rangeset();
	
	return 0;
}

static void liquid_exit(void)
{
	unregister_chrdev(UNNAMED_MAJOR, "pm");
	printk(KERN_ALERT "exit liquid.\n");
}

module_init(liquid_init);
module_exit(liquid_exit);
