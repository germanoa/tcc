#ifndef _PMDRV_H
#define _PMDRV_H

/* PM macros (Simulated PM) */
#define PMFAKE_ADDR 0x8000000
#define PMFAKE_SIZE 0x9fffff
#define PM_ADDR PMFAKE_ADDR
#define PM_SIZE PMFAKE_SIZE

#define PM_MAXVOLUMES 256

/* PM volume macros */
#define PM_AVAILABLE_ID 46464646
#define PM_ID_MAXSIZE 4
#define PM_SIZE_MAXSIZE 256
#define PM_FLAGS_MAXSIZE 4

/* PM volume flags macros */
#define PM_UNSET 0x1
#define PM_SET 0x2

#define PM_USED 0x4

#define PM_CONN_MEM 0x8
#define PM_CONN_PCIe 0x10
#define PM_SYNC_NONE 0x20
#define PM_SYNC_VADDR 0x40
#define PM_SYNC_PADDR 0x80

/* PM ERRORs macros */
#define PM_ERROR_IDINVAL -1
#define PM_ERROR_SIZE -2
#define PM_ERROR_MAXRANGES -3


/* PMDRV macros */
#define PMDRV_MGR PM_ADDR
#define PMDRV_MGR_SIZE 1024000
#define PMDRV_ACTIVE 0xf0f0f0f0 
#define PMDRV_DEBUG 1



/* PMDRV PROCFS macros */
#define PMDRV_PROC_DIR "pm"
#define PMDRV_PROC_ENTRY "volumes"
#define PMDRV_PROC_MAXSIZE 1000



/*
 * each PM will have, at its first PMDRV_MGR_SIZE bytes,
 * a manager area with administratively data:
 * a pmsuper_t at offset 0;
 * until PM_MAXRANGES pmrange_t, beginning at offset sizeof(pmsuper_t)
 */

struct pmvolume_t {
	struct list_head list; /*http://kernelnewbies.org/FAQ/LinkedLists*/
	unsigned int id;
	unsigned long phys_addr;
	unsigned int size;
	int flags;
};

struct pmsuper_t {
	int active; //if !PMDRV_ACTIVE, first use of PM
	unsigned int mgr_used; // control where alloc pmranges at PMDRV_MGR
	int count;
	struct list_head pmvolumes; // list head of pmvolumes
};

/* the head to pmdrv set configs and ranges */
struct pmsuper_t *super;

/* /proc/pm dir */
struct proc_dir_entry *pm_proc_root;

//to retrieve pmvolumes, as NVM ProgModel
struct pmvolume_t get_rangeset(unsigned int id);

int exists_pmvolume(unsigned int id);


#endif  /* _PMDRV_H */
