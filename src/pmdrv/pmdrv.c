#include <linux/init.h>
#include <linux/module.h>
#include <linux/vmalloc.h> 
#include <asm/io.h>
#include <linux/mman.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

#include </tcc/pmdrv/pmdrv.h>

MODULE_LICENSE("Dual BSD/GPL");


//struct proc_dir_entry *pm_proc_root;
char pmvolset[2048];
char pmvol[2048];

/* init string with 0's */
void static inline _init_str(char *s,int size)
{
	int i;
	for (i=0;i<size;++i) {
		s[i]=0;
	}
}

/* return pos of char from string */
int static inline _search_char(const char *s, char c, int size)
{
	int i,ret=0;
	for (i=0;i<size;++i) {
		if (s[i]==c) {
			ret=i;
			break;
		}
	}
	return ret;
}

int _int_from_string(const char *s,char split, int size)
{
	char temp[size+1];
	unsigned long ret,x;

	_init_str(temp,size+1);
	strncpy(temp,s,size);
	x = strict_strtoul(temp,10,&ret);
	if (x) {
		printk(KERN_ALERT "PMDRV:_int_from_string: kstrtoul returns %lu\n",x);
		ret=-1;
	}

	return ret;
}

void _super_clear(void)
{
	super->active = 0;
}

void pmvolume_print(struct pmvolume_t pos)
{
	if (pos.id==-1) {
		printk(KERN_INFO "PM AVAILABLE:\n");
	} else {
		printk(KERN_INFO "PMVOLUME ID: %d\n",pos.id);
	}
	printk(KERN_INFO "\tphys_addr: %lx\n",pos.phys_addr);
	printk(KERN_INFO "\tsize: %d\n",pos.size);
}

static struct pmvolume_t _pmvolume_null(void)
{
	struct pmvolume_t null;
	null.id = -2;
	null.phys_addr = 0;
	null.size = 0;
	null.flags = 0;
	return null;
}

int exists_pmvolume(unsigned int id)
{
	struct pmvolume_t *it = NULL;
	int ret=0;

	list_for_each_entry(it,&super->pmvolumes, list)
	{
		if (it->id == id) {
				ret=1;
				goto done;
			}
	}
done:	
	return ret;
}
EXPORT_SYMBOL(exists_pmvolume);


void _pmvolume_init(struct pmvolume_t **range, unsigned int addr_start, unsigned int size)
{
		*range = (struct pmvolume_t *) super + super->mgr_used;
		super->mgr_used += sizeof(struct pmvolume_t);
		super->count++;

		(*range)->id = PM_AVAILABLE_ID;
		(*range)->phys_addr = addr_start;
		(*range)->size = size;
		(*range)->flags = PM_UNSET;
		INIT_LIST_HEAD(&(*range)->list);
}

int _pmvolume_remove(unsigned int id)
{

	struct pmvolume_t *prev,*next,*it = NULL;
	struct list_head *l;
	int ret=0;

	list_for_each_entry(it,&super->pmvolumes, list)
	{
		if (it->id == id) {
				super->count--;
				ret=1;
				l = &it->list;
				prev = list_entry (l->prev,struct pmvolume_t, list);
				next = list_entry (l->next,struct pmvolume_t, list);
				printk("prev:%d\n",prev->id);
				printk("next:%d\n",next->id);
				if (prev->flags & PM_UNSET) {
					printk("oi\n");
				} else if (next->flags & PM_UNSET) {
					printk("oiw\n");
				} else {
					it->id=PM_AVAILABLE_ID;
				}
				goto done;
			}
	}

done:
	return ret;
}
//
//	struct pmvolume_t *prev,*next,*new;
//	struct list_head *l;
//	l = &(*cur)->list;
//	prev = list_entry (l->prev,struct pmvolume_t, list);
//	next = list_entry (l->next,struct pmvolume_t, list);
//	if (prev->flags & PM_UNSET) {
//		printk(KERN_ALERT "removendo entrou no if.\n");
//		if ((*cur)->phys_addr < prev->phys_addr) { /* when cur is the last volume */
//			prev->phys_addr= (*cur)->phys_addr;     /* of the linked list          */
//		}
//		prev->size+= (*cur)->size;
//		list_del(&(*cur)->list);
//	} else {
//		printk(KERN_ALERT "removendo ID=%u entrou no else.\n",(*cur)->id);
//		_pmvolume_init(cur,(*cur)->phys_addr,(*cur)->size);
//		printk(KERN_ALERT "333\n");
//		pmvolume_print(**cur);
//	}
//}

void _pmvolume_insert(unsigned int id, struct pmvolume_t *cur, unsigned int size, int flags)
{
	struct pmvolume_t *over = NULL;

	_pmvolume_init(&over, cur->phys_addr + size, cur->size - size);
	list_add_tail(&over->list,&super->pmvolumes);

	cur->id = id;
	cur->size = size;
	cur->flags = PM_SET;
	cur->flags |= flags;
}

int set_rangeset(unsigned int id, unsigned int size, int flags)
{
	struct pmvolume_t *pos = NULL;
	int ret=1;

	if(exists_pmvolume(id)) {
		printk(KERN_ALERT "PMDRV:set_rangeset: ID already exists\n");
		ret = PM_ERROR_IDINVAL;
		goto done;
	}

	//size must be >= page_size because maps are page align.
	if((size<PAGE_SIZE) || (size>PM_SIZE)) {
		printk(KERN_ALERT "PMDRV:set_rangeset: SIZE invalid.\n");
		ret = PM_ERROR_SIZE;
		goto done;
	}

	if(super->count>=PM_MAXVOLUMES) {
		printk(KERN_ALERT "set_rangeset: MAXVOLUMES exceeded.\n");
		ret = PM_ERROR_MAXRANGES;
		goto done;
	}

	if(flags==0) {
		flags = PM_CONN_MEM | PM_SYNC_NONE;
	}

	list_for_each_entry(pos,&super->pmvolumes, list)
	{
		if ((pos->flags & PM_UNSET)
				&& (pos->size>=size)) {
			//first fit
			_pmvolume_insert(id, pos, size, flags);
			goto done;
		}
	}
done:	
	return ret;
}

struct pmvolume_t **get_ptr_rangeset(unsigned int id)
{
	struct pmvolume_t *it = NULL;
	struct pmvolume_t **ret = NULL;

	list_for_each_entry(it,&super->pmvolumes, list)
	{
		if (it->id == id) {
			it->flags |= PM_USED;
			ret = &it;
			goto done;
		}
	}
done:	
	return ret;
}

struct pmvolume_t get_rangeset(unsigned int id)
{
	struct pmvolume_t *it = NULL;
	struct pmvolume_t ret = _pmvolume_null();

	list_for_each_entry(it,&super->pmvolumes, list)
	{
		if (it->id == id) {
			it->flags |= PM_USED;
			ret = *it;
			goto done;
		}
	}
done:	
	return ret;
}
EXPORT_SYMBOL(get_rangeset);


int pmdrv_proc_read(char *buffer,
	      char **buffer_location,
	      off_t offset, int buffer_length, int *eof, void *data)
{
	struct pmvolume_t *it = NULL;
	int n,n_ac=0;
	
	if (offset > 0) {
		/* we have finished to read, return 0 */
		n_ac  = 0;
	} else {
		/* fill the buffer, return the buffer size */

		_init_str(pmvol,2048);
		_init_str(pmvolset,2048);
		list_for_each_entry(it, &super->pmvolumes, list)
		{
			if (it->id==PM_AVAILABLE_ID) {
				n = sprintf(pmvol,"VOLUME AVAILABLE:\n\tphys_addr:%lX\n\tsize:%u\n",
					it->phys_addr,it->size);
			} else {
				n = sprintf(pmvol,"VOLUME ID:%u\n\tphys_addr:%lX\n\tsize:%u\n",
					it->id,it->phys_addr,it->size);
			}
			strncat(pmvolset,pmvol,n);
			_init_str(pmvol,n+1);
			n_ac+=n;
		}
		memcpy(buffer,pmvolset, n_ac);
	}

	return n_ac;
}


int pmdrv_proc_write(struct file *file, const char *buffer, unsigned long count,
		   void *data)
{
	unsigned long id,size,flags;
	int pos,pos_ac;
	char pmdrv_proc_buffer[PMDRV_PROC_MAXSIZE];
	unsigned long pmdrv_proc_buffer_size = 0;

	pmdrv_proc_buffer_size = count;
	if (pmdrv_proc_buffer_size > PMDRV_PROC_MAXSIZE ) {
		pmdrv_proc_buffer_size = PMDRV_PROC_MAXSIZE;
	}
	
	/* write data to the buffer */
	//if ( copy_from_user(pmdrv_proc_buffer, buffer, pmdrv_proc_buffer_size) ) {
	if ( copy_from_user(pmdrv_proc_buffer, buffer, sizeof(pmdrv_proc_buffer)) ) {
		return -EFAULT;
	}

	/* split pm volume info */
	pos = _search_char(pmdrv_proc_buffer,',',pmdrv_proc_buffer_size);
	id = _int_from_string(pmdrv_proc_buffer,',',pos);

	pos_ac=pos+1; //+1 because ','
	pos = _search_char(&pmdrv_proc_buffer[pos_ac],',',pmdrv_proc_buffer_size);
	size = _int_from_string(&pmdrv_proc_buffer[pos_ac],',',pos);

	pos_ac=pos+1; //+1 because ','
	pos = _search_char(&pmdrv_proc_buffer[pos_ac],',',pmdrv_proc_buffer_size);
	flags = _int_from_string(&pmdrv_proc_buffer[pos_ac],',',pos);

	if((id==-1) || (size==-1) || (flags==-1)) {
		printk(KERN_ALERT "PMDRV: format error.\n");
	} else {
		if(!set_rangeset(id,size,flags)) {
			printk(KERN_ALERT "PMDRV: set_rangeset error.\n");
		}
	}

	return pmdrv_proc_buffer_size;
}

void pmdrv_init_procfs(void)
{
	static struct proc_dir_entry *pmdrv_proc;

	pm_proc_root = proc_mkdir(PMDRV_PROC_DIR,NULL);
	pmdrv_proc = create_proc_entry(PMDRV_PROC_ENTRY, 0644, pm_proc_root);
	
	if (pmdrv_proc == NULL) {
		printk(KERN_ALERT "Error: Could not initialize /proc/%s/%s\n",
			PMDRV_PROC_DIR,PMDRV_PROC_ENTRY);
	}

	pmdrv_proc->read_proc  = pmdrv_proc_read;
	pmdrv_proc->write_proc = pmdrv_proc_write;
	pmdrv_proc->mode 	  = S_IFREG | S_IRUGO;

	printk(KERN_INFO "/proc/%s/%s created\n", PMDRV_PROC_DIR,PMDRV_PROC_ENTRY);	
}

static int pmdrv_init(void)
{
	struct pmvolume_t *first_range = NULL;

	// if PMDRV_DEBUG
	struct pmvolume_t *it = NULL;

	// avoiding page cache. see XIP
	super = (void __iomem*) ioremap_nocache(PMDRV_MGR,PMDRV_MGR_SIZE);

	_super_clear();	

	if (super->active != PMDRV_ACTIVE) {
		printk(KERN_INFO "init pmdrv first step.\n");
		super->active = PMDRV_ACTIVE;
		super->mgr_used = sizeof(struct pmsuper_t);
		super->count=1;
		INIT_LIST_HEAD(&super->pmvolumes);
		//first range represents all PM
		_pmvolume_init(&first_range,PM_ADDR+PMDRV_MGR_SIZE,PM_SIZE-PMDRV_MGR_SIZE);
		list_add_tail(&first_range->list,&super->pmvolumes);
	}

	pmdrv_init_procfs();

//	if (PMDRV_DEBUG) {
//		printk(KERN_INFO "PMDRV - PMVOLUMES:\n");
//		list_for_each_entry(it, &super->pmvolumes, list)
//		{
//			pmvolume_print(*it);
//		}
//	}

/*TESTETTTTT*/
	struct pmvolume_t *pos;
	list_for_each_entry(pos,&super->pmvolumes, list)
	{
		if ((pos->flags & PM_UNSET)
				&& (pos->size>=10000)) {
			//first fit
			_pmvolume_insert(1, pos, 10000, 0);
			break;
		}
	}

	list_for_each_entry(pos,&super->pmvolumes, list)
	{
		if ((pos->flags & PM_UNSET)
				&& (pos->size>=10000)) {
			//first fit
			_pmvolume_insert(2, pos, 10000, 0);
			break;
		}
	}

 	_pmvolume_remove(1);

	if (PMDRV_DEBUG) {
		list_for_each_entry(it, &super->pmvolumes, list)
		{
			pmvolume_print(*it);
		}
/*TESTETTTTT*/
	}

	return 0;
}

void pmdrv_exit_procfs(void)
{
	remove_proc_entry(PMDRV_PROC_ENTRY, pm_proc_root);
	remove_proc_entry(pm_proc_root->name, NULL);
}

static void pmdrv_exit(void)
{
	pmdrv_exit_procfs();	

	printk(KERN_INFO "exit pmdrv.\n");
}

module_init(pmdrv_init);
module_exit(pmdrv_exit);
