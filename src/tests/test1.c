#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>

int main(int argc, char *argv[])
{

	int fd;
	int *pm;
	int ret=0;

	if ((fd = open("/dev/pmraw1", O_RDWR) ) < 0) {
		printf("can't open /dev/pmraw1 \n");
		ret=1;
	}
	
	pm = (int *)mmap(NULL,sizeof(int)*8,PROT_READ|PROT_WRITE,MAP_FILE|
		MAP_SHARED|MAP_FIXED,fd,0);

	if ((long)pm < 0) {
		printf("mmap error \n");
		ret=1;
	} else {
		pm[0] = pm[0]+1;
		printf("pm: %d\n",*pm);
	}

	ret = msync(pm,sizeof(int)*8,0);
	printf("msync:%d\n",ret);

	return ret;
}
