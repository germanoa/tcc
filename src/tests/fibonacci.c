#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>

#define SIZE 512
#define FILENAME_SIZE 256

int main(int argc, char *argv[])
{

	int fd,ret,i;
	long n=0;
	long long *map;
  char filename[FILENAME_SIZE];

	for (i=0;i<FILENAME_SIZE;++i) {
		filename[i]=0;
	}

  if (argc!=3) {
    fprintf(stderr,"use: %s /dev/pmrawN fibo_n\n",argv[0]);
    ret = -1000;
  } else {
    size_t length = strlen(argv[1]) + 1;
    if (length>FILENAME_SIZE) {
      fprintf(stderr,"path/to/file with more than %d chars\n",FILENAME_SIZE);
      ret = -1001;
    } else {
    	strncpy(filename,argv[1],length);

			n = strtol(argv[2], NULL, 10);

  		if ((fd = open(filename, O_RDWR) ) < 0) {
  			printf("can't open %s\n",filename);
  			ret=-1;
  		}
  		
  		map = (long long *)mmap(NULL,sizeof(long long)*n,PROT_READ|PROT_WRITE,
				MAP_FILE|MAP_SHARED|MAP_FIXED,fd,0);
  
  		if ((long)map < 0) {
  			printf("mmap error \n");
  			ret=-1;
  		} else {

				if (n>0) {
  				printf("Sequencia de Fibonacci:\n");
  				map[0]=0;
  				printf("%d -> %llu\n",0,map[0]);
  				map[1]=1;
  				printf("%d -> %llu\n",1,map[1]);
  				for(i=2;i<=n;i++) {
  				   map[i]=map[i-1]+map[i-2];
  					 printf("%d -> %llu\n",i,map[i]);
  				}
  				ret=map[n-1];
  		  } else {
    			fprintf(stderr,"n=%lu thats a problem.\n",n);
				}
			}
		}
  }

  return ret;
}
